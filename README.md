[live tester / playground](http://perplexedpeach.gitlab.io/dynamic-avatar-drawer/dist/test.html)

[documentation / updates](http://perplexedpeach.gitlab.io/dynamic-avatar-drawer/)

Function
===
Model and View for characters in Javascript-enabled games and stories.

It provides:
 - procedural drawing of characters and clothing
 - inventory management and clothing layering
 - facial expression system
 - mixing body parts (e.g. elf ears on human head)
 - missing body parts (e.g. hand was cut off)
 - smooth animation for transformations as a result of stat changes
 - interface for defining custom stats and mapping those to appearance dimensions

Usage
===
Include `da.js` in your project and call methods on the `da` module.
It's found under `public/da.js` in this repository.

See top for link to usage and content creation guide.
For an API reference, see the `ref` directory of this repository or the link above.

Use the playground to generate live code and see their effects.

For development, you'll need to install some dependencies. First install the latest version of 
[node and npm](https://nodejs.org/en/download/)

Then open a console, navigate to the project root directory, then run
```bash
npm install
```

For development, run 
```bash
npm run start
```

To start a webdev server that will launch the demo, listen for file changes, recompile
on file changes, and refresh the demo after it's done recompiling.

For building `public/da.js` for release/production, run
```bash
npm run build
```

Local website build
===
Set up dependencies

1. install the latest version of ruby
2. install rubydevkit and point to the previous installation of ruby
3. navigate to portfolio root directory with git bash
4. `gem install bundler`
5. `bundle install`


1. `bundle exec jekyll serve`
