import {adjustColor, RGBToHSL} from "../util/utility";
import {Layer} from "../util/canvas";
import {incu} from "../util/draw";
import {clamp} from "drawpoint";

// export ex.baseStroke, ex.baseFill, and ex.baseLipColor
export function configureBaseColors(ex) {
    const avatar = ex.avatar;

    const skin = avatar.dim.skin;

    if (isNaN(skin)) {
        ex.baseFill = skin;
        ex.baseStroke = skin;
        ex.baseLipColor = skin;
        ex.nippleColor = skin;
        return;
    } else if (skin < 11) {
        ex.baseFill = [
            Math.floor(255 - (skin * 2.8)),
            Math.floor(214 - (skin * 5.3)),
            Math.floor(180 - (skin * 6.5))
        ];
        ex.baseStroke = [
            Math.floor(214 - (skin * 5.1)),
            Math.floor(156 - (skin * 4)),
            Math.floor(147 - (skin * 6.4))
        ];
        ex.baseLipColor = [
            Math.floor(194 - (skin * 4.1)),
            Math.floor(123 - (skin * 4.1)),
            Math.floor(119 - (skin * 4.1))
        ];
        ex.nippleColor = [
            Math.floor(140 - (skin * 4.1)),
            Math.floor(89 - (skin * 4.1)),
            Math.floor(86 - (skin * 4.1))
        ];
    } else if (skin < 100) {
        let a = skin - 11;
        ex.baseFill = [
            Math.floor(227 - (a * 9.6)),
            Math.floor(161 - (a * 9.1)),
            Math.floor(115 - (a * 6.3))
        ];
        if (skin > 28) {
            a = a - ((skin - 23) * 2.5);
        }  // change colours to lighter so we do not get black on
        // near black effects
        ex.baseStroke = [
            Math.floor(163 - (a * 12)),
            Math.floor(116 - (a * 10.8)),
            Math.floor(83 - (a * 7.3))
        ];
        ex.baseLipColor = [
            Math.floor(153 - (a * 8.9)),
            Math.floor(82 - (a * 6.2)),
            Math.floor(78 - (a * 6.4))
        ];
        ex.nippleColor = [
            Math.floor(99 - (a * 9.9)),
            Math.floor(48 - (a * 7.2)),
            Math.floor(45 - (a * 7.4))
        ];
    }

    const lipProminance = 0.7 + clamp(avatar.getDim("faceFem") /
                                      (avatar.getDimDesc("faceFem").high * 3), 0, 0.3);
    ex.baseLipColor.forEach(function (elem, index, arr) {
        arr[index] = ex.baseFill[index] * (1 - lipProminance) + elem * lipProminance;
    });

    // modify skin coloration by first converting to HSL
    ex.baseFill = finalizeColor(ex.baseFill, avatar.Mods, "skin");
    ex.baseStroke = finalizeColor(ex.baseStroke, avatar.Mods, "skin");
    ex.baseLipColor = finalizeColor(ex.baseLipColor, avatar.Mods);
    ex.nippleColor = finalizeColor(ex.nippleColor, avatar.Mods);

    const eyeHighlight = avatar.getDim("faceFem") / avatar.getDimDesc("faceFem").high * 20;
    ex.eyelidColor = adjustColor(ex.baseStroke,
        {
            s: -eyeHighlight,
            l: -eyeHighlight
        });

    ex.defaultHairFill =
        `hsl(${Math.round(avatar.dim.hairHue)},${Math.round(avatar.dim.hairSaturation)}%,
        ${Math.round(avatar.dim.hairLightness)}%)`;
    ex.hairFill = avatar.hairFill;
    ex.hairStroke = avatar.hairStroke;
    ex.browFill = avatar.browFill;
    ex.lashFill = avatar.lashFill;
    // in case we don't have overrides
    if (!avatar.hairFill) {
        ex.hairFill = ex.defaultHairFill;
    }
    if (!avatar.hairStroke) {
        ex.hairStroke = adjustColor(ex.defaultHairFill,
            {
                l: -10,
                s: -10
            });
    }
    if (!avatar.browFill) {
        ex.browFill = adjustColor(ex.defaultHairFill,
            {
                l: -5,
                s: -5
            });
    }
    if (!avatar.lashFill) {
        ex.lashFill = adjustColor(ex.defaultHairFill,
            {
                l: -10,
                s: -10,
            });
    }

    ex.hairAccessoryColor =
        `hsl(${avatar.getMod("hairAccessoryHue")},${avatar.getMod("hairAccessorySaturation")}%,${avatar.getMod(
            "hairAccessoryLightness")}%)`;
}


function finalizeColor(rgb, mods, modName) {
    const hsl = RGBToHSL({
        r: clamp(rgb[0], 0, 255),
        g: clamp(rgb[1], 0, 255),
        b: clamp(rgb[2], 0, 255)
    });
    if (modName) {
        hsl.h += mods[modName + "Hue"];
        hsl.s += mods[modName + "Saturation"];
        hsl.l += mods[modName + "Lightness"];
    }
    return "hsl(" + clamp(Math.floor(hsl.h), 0, 360) + "," +
           clamp(Math.floor(hsl.s), 0, 100) + "%," +
           clamp(Math.floor(hsl.l), 0, 100) + "%)";
}

export function initCanvas(canvas, ctx, config, avatar, ex, layer, clear) {
    // clear canvas
    if (clear) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
    // prevent canvas from shrinking...

    // use as much of the space as necessary
    // use the minimum scaling from x and y, then take the rest as offset
    const sx = canvas.width / 180, sy = canvas.height / 400;
    const scaling = ex.scaling = Math.min(sx, sy);
    ctx.scale(scaling, scaling);

    ex.ox = config.ox = Math.floor(canvas.width / scaling - canvas.width / sx);
    ex.oy = config.oy = Math.floor(canvas.height / scaling - canvas.height / sy);

    if (layer === Layer.BASE && config.printAdditionalInfo) {
        printAdditionalInfo(canvas, ctx, config, avatar);
    }


    ctx.translate(config.ox, 0);

    if (layer === Layer.BASE && config.printHeight) {
        printHeight(ctx, config, avatar);
    }

    // flip vertically
    ctx.scale(1, -1);
    // elevate by offset and shoe height (we divide by 2 to account for perspective)
    // whether this is necessary can be decided at a later time
    ctx.translate(0, -400 - config.oy + 10 + incu(avatar.heightAdjust()) / 2);
    // scale to be in sync with the height bar
    // height bar scale is 6 ft = 390-20 = 370 canvas units
    // but incu ~ 1 so we skip the following conversion to avoid antialiasing issues
    // ctx.scale(1, incu(72) / 370);

    // move so that the center x is 0
    // allow draw points to be relative to the center of the body
    ctx.translate(ex.cx, 0);

    // initialize to base skin colors
    ctx.strokeStyle = ex.baseStroke;
    ctx.fillStyle = ex.baseFill;
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
}


function printHeight(ctx, config, avatar) {
    const shoeheight = avatar.heightAdjust();
    const heightOffset = 2; // players appear 2 inches taller than what their code suggests

    // height is given in centimeters, so have to convert
    let heightread = (avatar.dim.height) / 2.54 + shoeheight + heightOffset;  // in inches
    let heightft = "" + Math.floor(heightread / 12) + "\'";
    let heightin = "" + Math.floor(heightread - (Math.floor(heightread / 12) * 12)) + "\"";

    const oy = config.oy;

    // print imperial height
    heightread = heightft + heightin;
    ctx.save();
    ctx.fillStyle = config.heightColor;
    ctx.font = "12px Arial";
    ctx.fillText(heightread, 125, 16);

    if (shoeheight > 0) {
        heightread = (avatar.dim.height) / 2.54 + heightOffset;  // unadjusted
        heightft = "" + Math.floor(heightread / 12) + "\'";
        heightin = "" + Math.floor(heightread - (Math.floor(heightread / 12) * 12)) + "\"";
        heightread = heightft + heightin;
        ctx.fillText("(" + heightread + ")", 150, 16);
    }


    // draw height measurement bar on the right
    ctx.strokeStyle = config.heightBarColor;
    ctx.beginPath();
    const pos = 20;
    ctx.moveTo(130 + pos, 20);
    ctx.lineTo(158 + pos, 20);
    ctx.lineTo(158 + pos, 390 + oy);
    const dashes = 370 / 72;
    const numdashes = Math.floor(72 + oy / dashes);
    let i = 1;
    for (i = 1; i < numdashes; i++) {
        var v = 390 + oy - (i * dashes);
        ctx.moveTo(158 + pos, v);
        if (i % 12 === 0) {
            ctx.lineTo(144 + pos, v);
        } else if (i % 6 === 0) {
            ctx.lineTo(151 + pos, v);
        } else if (i % 3 === 0) {
            ctx.lineTo(153 + pos, v);
        } else {
            ctx.lineTo(155 + pos, v);
        }
    }
    ctx.stroke();
    ctx.restore();
}

function printAdditionalInfo(canvas, ctx, config, avatar) {
    // print other info
    ctx.save();
    ctx.font = "bold 20px Arial";
    ctx.fillStyle = config.genderColor;
    ctx.fillText(avatar.isMale() ? String.fromCharCode(0x2642) : String.fromCharCode(0x2640), 6,
        24);
    const fontSize = clamp(canvas.width / avatar.name.length / 2, 10, 24);
    ctx.font = fontSize + "px Arial";

    ctx.fillStyle = config.nameColor;
    const nameLengthLimit = 24;
    if (avatar.name.length > nameLengthLimit) {
        ctx.fillText(avatar.name.slice(0, nameLengthLimit) + "...", 30, 26);
    } else {
        ctx.fillText(avatar.name, 30, 26);
    }
    ctx.restore();
}

