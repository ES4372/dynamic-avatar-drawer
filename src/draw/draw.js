import Context2DTracked from "context-2d-tracked";
import {drawTattooPattern, patternLoadingQueue} from "../util/pattern";
import {loaded} from "../load/load";
import {Skeleton} from "../skeletons/skeleton";
import {Part, getChildLocation, BodyPart} from "../parts/part";
import {extractBaseLocation, extractUnmodifiedLocation} from "../util/part";
import {convertPointsToCanvasUnits, setStrokeAndFill} from "../util/draw";
import {configureBaseColors, initCanvas} from "./init";
import {topologicalSort} from "../util/utility";
import {Layer, ShadingLayers} from "../util/canvas";
import {Clothes} from "../clothes/clothing";
import {
    simpleQuadratic,
    extractPoint,
    norm,
    diff,
    none,
    drawPoints,
    endPoint,
    rad, breakPoint
} from "drawpoint";
import {applyMods, removeMods} from "../util/player";
import {Items} from "../items/item";
import {ShoeSidePart} from "../clothes/shoes";
import {Tattoos} from "../decorative_parts/tattoo";

/* Copyright 2016 Johnson Zhong */


/**
 * Draw an avatar to a canvasGroup
 * @memberof module:da
 * @param {HTMLElement} canvasGroupObj HTML DOM element holding all the canvases, gotten with
 * da.getCanvasGroup
 * @param avatar Player object to draw
 * @param {object} userConfig Configuration object that contains
 * @param {boolean} userConfig.passThrough If true, will use existing dimensions to draw rather than calculating them
 * @param {string} userConfig.nameColor Color to render the name in
 * @param {string} userConfig.genderColor Color to render the gender in
 * @param {string} userConfig.heightColor Color to render the height text in
 * @param {string} userConfig.heightBarColor Color to render the height bar in
 * @returns {object} Exports
 */
export function draw(canvasGroupObj, avatar, userConfig) {
    if (!loaded) {
        throw new Error("trying to draw without having loaded first! (call da.load() before)");
    }
    // make sure all needed patterns are loaded
    if (patternLoadingQueue.length) {
        return Promise.all(patternLoadingQueue).then(() => {
            patternLoadingQueue.length = 0;
            return draw(canvasGroupObj, avatar, userConfig);
        });
    }
    // canvas name is the string id of the canvas element to draw to
    // if it's not a string, we assume it's the actual canvas passed in
    let config = {
        nameColor          : "#000",
        genderColor        : "#000",
        heightColor        : "#000",
        heightBarColor     : "#000",
        printAdditionalInfo: true,
        printHeight        : true,
        renderShoeSideView : true,
        offsetX            : 0,
        offsetY            : 0,
    };
    if (userConfig) {
        Object.assign(config, userConfig);
    }

    // assuming canvas group exists

    // not terribly elegant way of doing this (don't want to pass avatar to draw functions)
    avatar.Mods.shoeHeight = avatar.heightAdjust();
    // define stats
    if (!config.passThrough) {
        avatar.calcDimensions();
    }
    const sk = Skeleton[avatar.skeleton];
    if (!sk) {
        throw new Error("can't find skeleton with name " + avatar.skeleton);
    }

    // UNIT DEFINITION
    // internal model of 400x180 canvas
    // assuming max height is 200cm, 1 canvas unit = 0.5cm = 5mm
    const ex = {};
    // center x and y
    ex.cx = 80 + config.offsetX;
    ex.cy = 200 + config.offsetY;

    ex.avatar = avatar;
    // DRAW POINTS defined (x,y) for specific body points so we have unified calculations
    // points are defined in cm
    // these are calculated once in a core function and then referenced in other functions
    // they are the exported physical parameters and are defined in their respective draw
    // functions
    ex[Part.LEFT] = {};
    ex[Part.RIGHT] = {};


    // let each body part calculate and export its own draw points
    avatar.parts.forEach((part) => {
        calcPart(ex, part);
    });
    avatar.faceParts.forEach((part) => {
        calcPart(ex, part);
    });
    avatar.decorativeParts.forEach((part) => {
        // decorative parts require the existence of either a part or facePart in the same location
        let decorativeLocation = part.loc;
        if (avatar.getPartInLocation(decorativeLocation) ||
            avatar.getPartInLocation(decorativeLocation, avatar.faceParts)) {
            calcPart(ex, part);
        }
    });

    // fill in parts that are missing
    avatar.fillMissingDrawpoints(ex[Part.LEFT]);
    avatar.fillMissingDrawpoints(ex[Part.RIGHT]);

    // convert to drawable units
    convertPointsToCanvasUnits(ex[Part.LEFT]);
    convertPointsToCanvasUnits(ex[Part.RIGHT]);

    // calculate base body colors
    configureBaseColors(ex);

    // add in reverse order
    const canvasGroup = [];
    for (let i = canvasGroupObj.children.length - 1; i >= 0; --i) {
        canvasGroup.push(canvasGroupObj.children[i]);
    }
    ex.canvasGroup = canvasGroup;
    // actually work with the canvases now
    const ctxGroup = [];
    for (let i = 0; i < Layer.NUM_LAYERS; ++i) {
        let ctx = new Context2DTracked(canvasGroup[i].getContext("2d"));
        initCanvas(canvasGroup[i], ctx, config, avatar, ex, i, true);
        ctxGroup.push(ctx);
    }
    ex.height = canvasGroup[Layer.BASE].height;
    ex.width = canvasGroup[Layer.BASE].width;


    // clipping mask for the body indexed by [layer][side]
    // to be defined in renderBase
    ex.clip = {};

    return new Promise((resolve) => {
        // underlying skin
        renderBase(ctxGroup, ex);

        // render individual parts
        renderParts(ctxGroup, ex, config);

        ex.ctxGroup = ctxGroup;
        ex.ctx = new Context2DTracked(canvasGroup[Layer.NUM_LAYERS].getContext("2d"));
        // set up display canvas with same scaling and translation as used for drawing
        initCanvas(canvasGroup[Layer.NUM_LAYERS],
            ex.ctx,
            config,
            avatar,
            ex,
            Layer.NUM_LAYERS,
            false);
        resolve(ex);
        return ex;
    });

}

function calcPart(ex, part) {
    const avatar = ex.avatar;

    // wrap the body part's calc function
    if (part.hasOwnProperty("Mods")) {
        applyMods(avatar.Mods, part.Mods);
        avatar.calcDimensions();
    }
    // inside each call, 'this' should refer to the avatar's dimensions
    part.calcDrawPoints.call(avatar.dim, ex[part.side], avatar._clampedMods,
        true, part);
    // also define the points for the other side
    if (part.reflect) {
        part.calcDrawPoints.call(avatar.dim, ex[1 - part.side], avatar._clampedMods,
            true, part);
    }

    if (part.hasOwnProperty("Mods")) {
        removeMods(avatar.Mods, part.Mods);
        avatar.calcDimensions();
    }
}

/**
 * guiMenuItem should be an array of part names
 * while drawnParts should have all the draw points associated with that part
 */
function synthesizeDrawPoints(partNames, drawnParts) {
    const dps = [];
    for (let p = 0; p < partNames.length; ++p) {
        dps.extend(drawnParts[partNames[p]]);
    }
    return dps;
}


export function renderBase(ctxGroup, ex) {

    // first pass, fill in base color
    for (let layer = Layer.BASE; layer < Layer.NUM_LAYERS; ++layer) {
        ex.clip[layer] = {};
        let ctx = ctxGroup[layer];

        ctx.save();
        // left side
        ctx.scale(-1, 1);
        ctx.beginPath();
        ex.clip[layer][Part.LEFT] = drawPartsLayer(ex, layer, Part.LEFT, ctx);
        ctx.fill();
        ctx.restore();

        // right side and center
        ex.clip[layer][Part.RIGHT] = drawPartsLayer(ex, layer, Part.RIGHT, ctx);
        ctx.fill();
    }
}

function strokePart(ctx, dps) {
    ctx.beginPath();
    drawPoints.apply(null, [ctx].concat(dps));
    ctx.stroke();
}

/**
 * Topologically sort parts so that they're drawn in a preferred order (above whatever they want)
 * @param avatar
 * @returns {*}
 */
function orderParts(avatar) {
    const partGroups = [
        "parts",
        "faceParts",
        "decorativeParts",
        "hairParts",
        "clothingParts",
        "shadingParts",
        "tattoos", // rendered as if they are parts (kind of like decorativeParts)
    ];

    // index -> part object
    const partMap = [];
    // part object -> index
    const partMapInverse = new Map();
    const partTypes = [];
    let index = 0;
    const partDependency = [];

    // initialize map data structures
    partGroups.forEach((group) => {
        avatar[group].forEach((part) => {
            partMap[index] = part;
            partMapInverse.set(part, index);
            partTypes.push(group);
            ++index;
        });
    });


    // find part-based dependencies
    partGroups.forEach((group) => {
        avatar[group].forEach((part) => {
            findDependencies(part);
        });
    });

    findClothingLayerDependencies();
    findClothingSameLayerOrdering();

    // find drawing-layer based dependencies
    // clothing parts occupying same location need to be ordered with inner layer drawn first
    function findClothingLayerDependencies() {
        const clothingLayers = [];

        for (let layer = 0; layer < Clothes.Layer.NUM_LAYERS; ++layer) {
            clothingLayers.push([]);
        }

        avatar["clothingParts"].forEach((clothingPart) => {
            const layer = clothingPart._owner.clothingLayer;
            clothingLayers[layer].push(clothingPart);
        });
        // draw all clothes of a lower layer before outer layer
        for (let layer = 0; layer < clothingLayers.length; ++layer) {
            for (let outerLayer = layer + 1; outerLayer < clothingLayers.length; ++outerLayer) {
                clothingLayers[layer].forEach((innerPart) => {
                    clothingLayers[outerLayer].forEach((outerPart) => {
                        // still have to respect drawing layers
                        if (innerPart.layer <= outerPart.layer) {
                            partDependency.push([
                                partMapInverse.get(outerPart),
                                partMapInverse.get(innerPart)
                            ]);
                        }
                    });
                });
            }
        }
    }

    function findClothingSameLayerOrdering() {
        // cache clothing layers of all parts as well
        const clothingPartLayers = {};
        avatar["clothingParts"].forEach((clothingPart) => {
            const layer = clothingPart._owner.clothingLayer;
            if (clothingPartLayers.hasOwnProperty(layer)) {
                clothingPartLayers[layer].push(clothingPart);
            } else {
                clothingPartLayers[layer] = [clothingPart];
            }
        });

        // also determine order inside each clothing layer
        for (let layer in clothingPartLayers) {
            if (clothingPartLayers.hasOwnProperty(layer) === false) {
                continue;
            }
            const partsInLayer = clothingPartLayers[layer];
            partsInLayer.forEach((clothingPart) => {
                // if it doesn't have any ordering, skip
                if (!clothingPart.aboveSameLayerParts && !clothingPart.belowSameLayerParts) {
                    return;
                }
                partsInLayer.forEach((otherClothingPart) => {
                    if (otherClothingPart === clothingPart ||
                        otherClothingPart.layer !== clothingPart.layer) {
                        return;
                    }
                    const otherPartLoc = extractBaseLocation(extractUnmodifiedLocation(
                        otherClothingPart.loc));

                    // both have same precedence, no sort
                    if (mutualSameLayerOrdering(clothingPart.aboveSameLayerParts,
                        otherClothingPart.aboveSameLayerParts) ||
                        mutualSameLayerOrdering(clothingPart.belowSameLayerParts,
                            otherClothingPart.belowSameLayerParts)) {
                        return;
                    }

                    // draw the other clothing part first
                    if (clothingPart.aboveSameLayerParts &&
                        clothingPart.aboveSameLayerParts.indexOf(otherPartLoc) > -1) {
                        partDependency.push([
                            partMapInverse.get(clothingPart),
                            partMapInverse.get(otherClothingPart)
                        ]);
                    }
                    // draw self first
                    if (clothingPart.belowSameLayerParts &&
                        clothingPart.belowSameLayerParts.indexOf(otherPartLoc) > -1) {
                        partDependency.push([
                            partMapInverse.get(otherClothingPart),
                            partMapInverse.get(clothingPart),
                        ]);
                    }
                });
            });
        }

        function mutualSameLayerOrdering(sameLayerOrdering, otherSameLayerOrdering) {
            let samePrecedence = false;
            if (sameLayerOrdering && otherSameLayerOrdering) {
                sameLayerOrdering.forEach((partLoc) => {
                    if (otherSameLayerOrdering.indexOf(partLoc) > -1) {
                        samePrecedence = true;
                    }
                });
            }
            return samePrecedence;
        }
    }

    function findDependencies(part) {
        const partIndex = partMapInverse.get(part);
        // child parts should always be drawn after parent parts so as not to be covered
        if (part.childParts) {
            part.childParts.forEach((childBaseLocation) => {
                const {childLoc} = getChildLocation(part.loc, childBaseLocation);
                const childPart = avatar.getPartInLocation(childLoc, avatar.parts);
                if (childPart !== null) {
                    partDependency.push([partMapInverse.get(childPart), partIndex]);
                }
            });
        }

        if (part.aboveParts) {
            part.aboveParts.forEach((belowPartLoc) => {
                let groupConstraint = null;

                // has a constraint on which group the dependency is on
                if (belowPartLoc.indexOf(" ") > -1) {
                    const words = belowPartLoc.split(" ");

                    // assume there's only 1 space for now
                    groupConstraint = words[0];
                    belowPartLoc = words[1];
                }

                partGroups.forEach((group) => {
                    if (groupConstraint && groupConstraint !== group) {
                        return;
                    }
                    avatar[group].forEach((belowPart) => {
                        if (belowPart === part || belowPart.layer > part.layer) {
                            return;
                        }
                        if (belowPartLoc ===
                            extractBaseLocation(extractUnmodifiedLocation(belowPart.loc))) {
                            partDependency.push([partIndex, partMapInverse.get(belowPart)]);
                        }
                    });
                });
            });
        }

        if (part.belowParts) {
            part.belowParts.forEach((abovePartLoc) => {
                let groupConstraint = null;

                // has a constraint on which group the dependency is on
                if (abovePartLoc.indexOf(" ") > -1) {
                    const words = abovePartLoc.split(" ");

                    // assume there's only 1 space for now
                    groupConstraint = words[0];
                    abovePartLoc = words[1];
                }

                partGroups.forEach((group) => {
                    if (groupConstraint && groupConstraint !== group) {
                        return;
                    }
                    avatar[group].forEach((abovePart) => {
                        if (abovePart === part || part.layer > abovePart.layer) {
                            return;
                        }
                        if (abovePartLoc ===
                            extractBaseLocation(extractUnmodifiedLocation(abovePart.loc))) {
                            // 2nd index should be drawn before first
                            partDependency.push([partMapInverse.get(abovePart), partIndex]);
                        }
                    });
                });
            });
        }
    }


    const indices = [];
    for (let i = 0; i < index; ++i) {
        indices.push(i);
    }
    // need to draw the ones "above" another one afterwards, so reverse ordering
    const partOrder = topologicalSort(indices, partDependency).reverse();
    const orderedParts = [];
    const orderedPartTypes = [];
    for (let p = 0; p < partOrder.length; ++p) {
        const partIndex = partOrder[p];
        orderedParts.push(partMap[partIndex]);
        orderedPartTypes.push(partTypes[partIndex]);
    }
    return {
        orderedPartTypes,
        orderedParts
    };
}

export function renderParts(ctxGroup, ex, config) {
    let avatar = ex.avatar;
    const {orderedPartTypes, orderedParts} = orderParts(avatar);

    // to be drawn separately
    const shoeSideParts = [];
    let drawnParts = {};
    for (let p = 0; p < orderedParts.length; ++p) {
        let part = orderedParts[p];
        if (part.coverConceal && part.coverConceal.length) {
            const partCovered = avatar.checkPartCoveredByClothing(part);

            // don't draw if there's any clothing covering this location
            if (partCovered) {
                continue;
            }
        }

        // populate drawn part for type if not exist yet
        if (drawnParts.hasOwnProperty(orderedPartTypes[p]) === false) {
            drawnParts[orderedPartTypes[p]] = {};
        }

        // depending on the part type, render using different methods
        const drawnPartsForType = drawnParts[orderedPartTypes[p]];

        // force redraw if we've drawn this part already as a child part
        // (we want ALL individual parts drawn)
        if (drawnPartsForType.hasOwnProperty(part.loc)) {
            drawnPartsForType[part.loc] = null;
        }

        switch (orderedPartTypes[p]) {
            // drop down
        case "parts":
        case "faceParts":
        case "decorativeParts":
            renderPart(part, p, drawnPartsForType);
            break;
        case "shadingParts":
            renderShading(part, p, drawnPartsForType);
            break;
        case "hairParts":
            renderHairPart(part);
            break;
        case "clothingParts":
            renderClothingPart(part);
            break;
        case "tattoos":
            renderTattoo(part);
            break;
        default:
            break;
        }
    }

    // before the rendering of shading forces us to remove
    renderItems();
    // also resets ctx transformations
    synthesizeLayers();
    if (config.renderShoeSideView) {
        renderShoeSideView();
    }

    function synthesizeLayers() {
        const displayCanvas = ex.canvasGroup[Layer.NUM_LAYERS];
        const displayCtx = displayCanvas.getContext("2d");
        displayCtx.setTransform(1, 0, 0, 1, 0, 0);
        displayCtx.clearRect(0, 0, displayCanvas.width, displayCanvas.height);

        const shadingCanvas = document.createElement("canvas");
        shadingCanvas.width = ex.canvasGroup[0].width;
        shadingCanvas.height = ex.canvasGroup[0].height;
        const shadingCtx = shadingCanvas.getContext("2d");

        for (let layer = Layer.BASE; layer < Layer.NUM_LAYERS; ++layer) {
            if (ShadingLayers.indexOf(layer) < 0) {
                displayCtx.globalCompositeOperation = "source-over";
                displayCtx.drawImage(ex.canvasGroup[layer], 0, 0);
            } else {
                shadingCtx.clearRect(0, 0, shadingCanvas.width, shadingCanvas.height);
                shadingCtx.drawImage(ex.canvasGroup[layer], 0, 0);
                // clip to layers up to now
                shadingCtx.globalCompositeOperation = "destination-in";
                shadingCtx.drawImage(displayCanvas, 0, 0);

                displayCtx.globalCompositeOperation = "multiply";
                displayCtx.drawImage(shadingCanvas, 0, 0);
                shadingCtx.globalCompositeOperation = "source-over";
            }
        }
    }


    function renderItems() {
        const position = {
            x: -ex.cx - ex.ox + 5,
            y: ex.height * 0.15 / 2.5
        };
        avatar.items.forEach((item) => {
            const image = Items.getItemRender(item);
            let width = image.width;
            let height = image.height;
            // overriding with width and we should scale our height similarly
            if (item.width) {
                height *= item.width / width;
                width = item.width;
            }

            const ctx = ctxGroup[item.layer];
            ctx.scale(1, -1);

            const renderPosition = item.renderItemLocation(ex, width, height);
            if (renderPosition) {
                ctx.drawImage(image, renderPosition.x, -renderPosition.y, width, -height);
            } else {
                ctx.drawImage(image, position.x, -position.y, width, -height);
                position.y += height;
            }
            ctx.scale(1, -1);
        });
    }

    function renderTattoo(tattoo) {
        const image = Tattoos.getRender(tattoo);
        const ctx = ctxGroup[tattoo.layer];
        const renderPosition = tattoo.renderTattooLocation(ex[tattoo.side]);

        ctx.save();
        if (tattoo.side === Part.LEFT) {
            ctx.scale(-1, 1);
        }

        if (!tattoo.ignoreClip) {
            // clip just on one side
            const clipFillPath = ex.clip[tattoo.layer][tattoo.side];
            clipPart(ctx, clipFillPath);
        }

        drawTattooPattern(tattoo, image, ctx, renderPosition);

        // note tattoos can't be on both sides of the body (so have to split center tattoo in 2)
        ctx.restore();
    }

    // draw side parts of shoes
    function renderShoeSideView() {
        const displayCanvas = ex.canvasGroup[Layer.NUM_LAYERS];
        const shoeSideCtx = displayCanvas.getContext("2d");
        shoeSideCtx.save();
        shoeSideCtx.translate(ex.ox, ex.height - 30);
        const scaling = ex.scaling;
        shoeSideCtx.scale(scaling, -scaling);
        const shoeBoxWidth = 220 / scaling;
        const shoeBoxHeight = ex.height * 0.2 / scaling;
        // translate to correct location
        shoeSideCtx.rect(0, 0, shoeBoxWidth, shoeBoxHeight);
        shoeSideCtx.clip();
        shoeSideParts.forEach((part) => {
            const side = part.side;
            shoeSideCtx.save();

            part.renderShoeSidePart.call(part._owner,
                ex[side],
                shoeSideCtx,
                avatar._clampedMods);
            shoeSideCtx.restore();
        });
        shoeSideCtx.restore();
    }

    function renderShading(part, partIndex, drawnParts) {
        // shading layers should be 1 layer above
        const ctx = ctxGroup[part.layer + 1];

        ctx.save();

        // needs to be drawn twice if need reflection
        if (part.side === Part.LEFT || part.reflect === true) {
            ctx.scale(-1, 1);
        }

        // configure strokes and fills
        setStrokeAndFill(ctx, part, ex);

        ctx.save();
        // clip so that fills don't spill outside body
        if (part.clipFill) {
            clipPart(ctx, part.clipFill.call(avatar.dim, ex[part.side]));
        }

        // get the draw points for this part
        drawPart(ex, orderedParts, partIndex, part.layer, part.side, drawnParts);
        // fill
        fillPart(ctx, drawnParts[part.loc]);
        strokePart(ctx, drawnParts[part.loc]);
        ctx.restore();

        if (part.reflect === true) {
            ctx.scale(-1, 1);

            if (part.clipFill) {
                clipPart(ctx, part.clipFill.call(avatar.dim, ex[part.side]));
            }
            // fill
            fillPart(ctx, drawnParts[part.loc]);
            strokePart(ctx, drawnParts[part.loc]);
        }

        ctx.restore();
    }


    function renderPart(part, partIndex, drawnParts) {
        if (!part.calcDrawPoints) {
            return;
        }
        const ctx = ctxGroup[part.layer];

        ctx.save();

        // needs to be drawn twice if need reflection
        if (part.side === Part.LEFT || part.reflect === true) {
            ctx.scale(-1, 1);
        }

        // configure strokes, fills, and lineWidth for this part
        setStrokeAndFill(ctx, part, ex);
        ctx.lineWidth = part.getLineWidth(avatar);

        // clip so that fills don't spill outside body
        let clipFillPath = part.clipFill ? part.clipFill(ex[part.side]) :
            ex.clip[part.layer][part.side];
        ctx.save();
        clipPart(ctx, clipFillPath);

        // get the draw points for this part
        drawPart(ex, orderedParts, partIndex, part.layer, part.side, drawnParts);
        // fill
        fillPart(ctx, drawnParts[part.loc]);
        ctx.restore();

        let toStroke = true;
        if (typeof part.stroke === "function" && part.stroke.length === 0) {
            toStroke = (part.stroke() !== none);
        } else {
            toStroke = (part.stroke !== none);
        }
        if (toStroke) {
            // stroke (need to restore and begin to reset clipping)
            // exceptionally clip stroke (usually we don't clip stroke)
            if (part.clipStroke) {
                let clipStrokePath = part.clipStroke(ex[part.side]);
                ctx.save();
                clipPart(ctx, clipStrokePath);
                strokePart(ctx, drawnParts[part.loc]);
                ctx.restore();
            } else {
                strokePart(ctx, drawnParts[part.loc]);
            }
        }


        if (part.reflect === true) {
            ctx.scale(-1, 1);

            ctx.save();
            clipPart(ctx, clipFillPath);

            // fill
            fillPart(ctx, drawnParts[part.loc]);

            // stroke (need to restore and begin to reset clipping)
            ctx.restore();

            if (toStroke) {
                if (part.hasOwnProperty("clipStroke")) {
                    let clipStrokePath = part.clipStroke(ex[part.side]);
                    ctx.save();
                    clipPart(ctx, clipStrokePath);
                    strokePart(ctx, drawnParts[part.loc]);
                    ctx.restore();
                } else {
                    strokePart(ctx, drawnParts[part.loc]);
                }

            }
        }

        ctx.restore();
    }

    function renderHairPart(part) {
        const ctx = ctxGroup[part.layer];
        ctx.save();
        setStrokeAndFill(ctx, part, ex);
        // also pass in extra coloring options
        part.renderHairPoints.call(avatar.dim, ctx, ex[part.side], avatar._clampedMods, ex);
        if (part.reflect) {
            ctx.save();
            ctx.scale(-1, 1);
            part.renderHairPoints.call(avatar.dim,
                ctx,
                ex[1 - part.side],
                avatar._clampedMods,
                ex);
            ctx.restore();
        }
        ctx.restore();
    }

    function renderClothingPart(part) {
        if (part instanceof ShoeSidePart) {
            shoeSideParts.push(part);
            return;
        }
        const ctx = ctxGroup[part.layer];
        const side = part.side;
        ctx.save();

        if (part.side === Part.LEFT) {
            ctx.scale(-1, 1);
        }
        // we don't set stroke and fill here since we don't know what properties the part
        // requires from the clothes
        part.renderClothingPoints.call(part._owner,
            ex[side],
            ctx,
            avatar._clampedMods, avatar);
        ctx.restore();
        if (part.reflect) {
            ctx.save();
            ctx.scale(-1, 1);
            part.renderClothingPoints.call(part._owner,
                ex[1 - side],
                ctx,
                avatar._clampedMods, avatar);
            ctx.restore();
        }
    }
}

export function drawPart(ex, parts, partIndex, layer, side, drawnParts) {
    const part = parts[partIndex];
    // error report
    if (typeof part.layer !== "number") {
        console.log(part.loc, "in layer", part.layer, "not found typeof",
            typeof Layer[part.layer]);
        return;
    }
    // don't draw if not part of layer, otherwise allow if needs to be reflected
    if (!part.calcDrawPoints || part.layer !== layer) {
        return;
    }
    if (part.reflect === false && (part.side !== side || drawnParts[part.loc])) {
        return;
    }


    // get actual draw points
    const avatar = ex.avatar;
    if (part.hasOwnProperty("Mods")) {
        applyMods(avatar.Mods, part.Mods);
        avatar.calcDimensions();
    }

    const owner = (part._owner) ? part._owner : avatar.dim;
    // inside each call, 'this' should refer to the avatar's dimensions
    const points = part.calcDrawPoints.call(owner, ex[side], avatar._clampedMods,
        false, part, avatar);

    if (part.hasOwnProperty("Mods")) {
        removeMods(avatar.Mods, part.Mods);
        avatar.calcDimensions();
    }


    // points that will eventually get drawn
    const drawnPoints = [];
    for (let p = 0; p < points.length; ++p) {
        // means we should draw all points of this part's child here (in between its other
        // points)
        if (points[p] && points[p].hasOwnProperty("child")) {

            const {childLoc, childSide} = getChildLocation(part.loc, points[p].child);

            if (drawnParts.hasOwnProperty(childLoc) === false) {
                // haven't drawn child part yet, we'll draw it now
                // first find the child part
                for (let childIndex = 0; childIndex < parts.length; ++childIndex) {
                    if (parts[childIndex].loc === childLoc &&
                        parts[childIndex] instanceof BodyPart) {
                        drawPart(ex, parts, childIndex, layer, childSide, drawnParts);
                        break;
                    }
                }

                // if couldn't find child, just ignore
            }

            // now we know either we've drawn the child before or we've just drawn it
            drawnPoints.extend(drawnParts[childLoc]);
            // clear child drawn points so it doesn't get drawn twice
            drawnParts[childLoc] = [];
        } else {
            // own point, just push
            drawnPoints.push(points[p]);
        }
    }
    // store for later usage (synthesis)
    drawnParts[part.loc] = drawnPoints;
    return drawnPoints;
}


/**
 * draw all avatar body parts on this layer and side (but not stroke or fill it)
 * returns all drawpoints for this layer and side
 */
export function drawPartsLayer(ex, layer, side, ctx) {
    // we load points onto here so that they can be drawn in order
    // initially we'll load the name of parts we want, then we synthesize the points
    // afterwards
    let dps = [];
    // shared storage for all the actually drawn parts
    const drawnParts = {};
    for (let i = 0; i < ex.avatar.parts.length; ++i) {
        drawPart(ex, ex.avatar.parts, i, layer, side, drawnParts);

        const part = ex.avatar.parts[i];
        if (ex.avatar.checkPartCoveredByClothing(part) === false) {
            dps.push(ex.avatar.parts[i].loc);
        }
    }
    dps = synthesizeDrawPoints(dps, drawnParts);
    drawPoints.apply(null, [ctx].concat(dps));
    return dps;
}

function clipPart(ctx, clipPath) {
    if (!clipPath) {
        return;
    }
    ctx.beginPath();
    drawPoints.apply(null, [ctx].concat(clipPath));
    ctx.closePath();
    ctx.clip();
}

function fillPart(ctx, points) {
    ctx.beginPath();
    const fillPoints = [];
    points.forEach((point) => {
        if (point && point.hasOwnProperty("fillOnly")) {
            fillPoints.push(...point.fillOnly);
        } else {
            fillPoints.push(point);
        }
    });
    // expand any fill only parts

    drawPoints.apply(null, [ctx].concat(fillPoints));

    let firstPoint = null;
    let lastPoint = fillPoints[fillPoints.length - 1];

    // force end point to bypass "smart fill" feature
    if (lastPoint === endPoint) {
        ctx.fill();
        return;
    }


    // want to make the part seem 3d and the first and last point aren't connected
    for (let p = 0; p < fillPoints.length; ++p) {
        if (fillPoints[p] && fillPoints[p].hasOwnProperty("x")) {
            firstPoint = fillPoints[p];
            break;
        }
    }
    for (let p = fillPoints.length - 1; p >= 0; --p) {
        if (fillPoints[p] && fillPoints[p].hasOwnProperty("x")) {
            lastPoint = fillPoints[p];
            break;
        }
    }
    if (firstPoint !== null) {
        const endPoint = connectEndPoints(lastPoint, firstPoint);
        drawPoints(ctx, null, endPoint);
        ctx.fill();
    }
}

export function connectEndPoints(firstPoint, lastPoint, deflection = 0.25) {
    let endPoint = extractPoint(lastPoint);
    const myDiff = diff(firstPoint, lastPoint);
    endPoint.cp1 =
        simpleQuadratic(firstPoint, lastPoint, 0.5, norm(myDiff) * deflection);
    return endPoint;
}

export function coverNipplesIfHaveNoBreasts(ex, ctx, part) {
    if (ex.hasOwnProperty("breast") === false) {
        ctx.save();
        ctx.lineWidth = 10;
        setStrokeAndFill(ctx,
            {
                fill  : none,
                stroke: part.fill
            },
            ex);
        ctx.beginPath();
        drawPoints(ctx, breakPoint, ex.chest.nipples);
        ctx.stroke();
        ctx.restore();
        return true;
    }
    return false;
}

export function drawFocusedWindow(focusedCanvas, ex, userConfig) {
    const ctx = focusedCanvas.getContext('2d');
    ctx.clearRect(0, 0, focusedCanvas.width, focusedCanvas.height);
    // the rendering canvas is the last canvas
    const canvas = ex.ctx.canvas;

    // convert these back to canvas coordinates since we allow users to specify in cm
    const sx = userConfig.center.x - userConfig.width / 2;
    const sy = userConfig.center.y + userConfig.height / 2;
    const spt = ex.ctx.tf.last().applyToPoint(sx, sy);

    const w = userConfig.width * ex.scaling;
    const h = userConfig.height * ex.scaling;

    // consider scaling when aspect ratio of target and source are different
    // keep height but give up some width if aspect ratio is not the same
    const destWidth = Math.min(focusedCanvas.width, focusedCanvas.height / h * w);
    ctx.drawImage(canvas,
        spt.x,
        spt.y,
        w,
        h,
        0,
        0,
        destWidth,
        focusedCanvas.height);
}