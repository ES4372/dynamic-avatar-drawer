import {FacePart} from "./face_part";
import {
    breakPoint,
    adjust,
    drawCircle,
} from "drawpoint";

class Iris extends FacePart {
    constructor(...data) {
        super({
            loc       : "iris",
            aboveParts: ["eyes"],
        }, ...data);
    }
}


export class IrisHuman extends Iris {
    constructor(...data) {
        super(...data);
    }

    stroke() {
        return "#000";
    }

    fill(ignore, ex) {

        let h = ex.avatar.Mods.irisHue;
        let s = ex.avatar.Mods.irisSaturation;
        let l = ex.avatar.Mods.irisLightness;
        return `hsl(${h},${s}%,${l}%)`;
    }

    getLineWidth(avatar) {
        return avatar.Mods.limbalRingSize * 0.01;
    }

    clipStroke(ex) {
        return this.clipFill(ex);
    }

    clipFill(ex) {
        return [ex.eyes.in, ex.eyes.top, ex.eyes.out, ex.eyes.in];
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            ex.eyes.iris = adjust(ex.eyes.center, 0, mods.irisHeight * 0.1);
        }
        return [
            breakPoint,
            ...drawCircle(ex.eyes.iris,
                mods.irisSize * 0.1)
        ];
    }
}


