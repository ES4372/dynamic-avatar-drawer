import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, drawCircle, breakPoint, extractPoint} from "drawpoint";
import {adjustColor} from "../util/utility";

export class BeautyMark extends DecorativePart {
    constructor(...data) {
        super({
            // offset relative to mouth
            offsetX   : -10,
            offsetY   : 10,
            radius    : 0.3,
            // for multiple moles, should prefix with number as in "1 head"
            loc       : "head",
            layer     : Layer.BELOW_HAIR,
            aboveParts: ["parts head"],
        }, ...data);
    }

    stroke() {
        return none;
    }

    clipFill() {

    }

    fill(ignore, ex) {
        return adjustColor(ex.baseStroke,
            {
                s: -30,
                l: -30
            });
    }

    calcDrawPoints(ex, ignore, calculate, part) {
        const center = extractPoint(ex.lips.center);
        center.x += part.offsetX;
        center.y += part.offsetY;
        return [breakPoint, ...drawCircle(center, part.radius)];
    }
}
