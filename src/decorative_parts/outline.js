import {DecorativePart} from "./decorative_part";
import {Layer} from "../util/canvas";
import {none, clamp, simpleQuadratic, breakPoint, splitCurve} from "drawpoint";
import {inheritStroke, inheritFill} from "../util/draw";

class Outline extends DecorativePart {
    constructor(...data) {
        super(...data);
    }

    stroke(ctx, ex) {
        return inheritStroke.call(this, ctx, ex);
    }

    fill() {
        return none;
    }
}


class MuscleOutline extends Outline {
    constructor(...data) {
        super(...data);
    }

    getLineWidth(avatar) {
        return clamp((avatar.getDim(this.muscleGroup) - 10) * 0.05, 0, 1.2);
    }

}


export class BellyButtonOutline extends Outline {
    constructor(...data) {
        super({
            loc       : "+torso",
            aboveParts: ["parts torso"],
            belowParts: ["clothingParts leg"],
            layer     : Layer.FRONT,
        }, ...data);
    }

    getLineWidth() {
        return 1;
    }

    calcDrawPoints(ex, ignore, calculate) {
        if (calculate) {
            let belly = ex.bellybutton = {};
            belly.top = {
                x: 0,
                y: ex.waist.y * 0.45 + ex.hip.y * 0.55
            };
            belly.bot = {
                x: belly.top.x,
                y: belly.top.y - 1
            };
            belly.bot.cp1 = simpleQuadratic(belly.top, belly.bot, 0.5, 0.5);
        }
        return [ex.bellybutton.top, ex.bellybutton.bot];
    }
}


export class DeltoidsOutline extends MuscleOutline {
    constructor(...data) {
        super({
            loc        : "+arm",
            aboveParts : ["parts arm"],
            layer      : Layer.FRONT,
            muscleGroup: "upperMuscle",
        }, ...data);
    }


    calcDrawPoints(ex, ignore, calculate) {
        if (this.upperMuscle < 20 || ex.hasOwnProperty("deltoids") === false) {
            return [];
        }
        if (calculate) {
            const bulk = this.upperMuscle - 10;
            let deltoids = ex.deltoids;
            deltoids.top = {
                x: ex.armpit.x,
                y: ex.armpit.y + 2
            };
            deltoids.bot = {
                x: deltoids.x - 5,
                y: deltoids.y
            };
            deltoids.bot.cp1 = simpleQuadratic(deltoids.top, deltoids.bot, 0.7, -bulk * 0.1);
        }
        return [ex.deltoids.top, ex.deltoids.bot];
    }
}


export class CollarboneOutline extends MuscleOutline {
    constructor(...data) {
        super({
            loc        : "+torso",
            layer      : Layer.FRONT,
            reflect    : true,
            muscleGroup: "upperMuscle",
        }, ...data);
    }

    calcDrawPoints(ex, ignore, calculate) {
        if (this.upperMuscle < 20 || ex.hasOwnProperty("collarbone") === false) {
            return [];
        }
        if (calculate) {
            let collarbone = ex.collarbone;
            collarbone.out = {
                x: collarbone.x - 0.5,
                y: collarbone.y - 2
            };
            collarbone.in = {
                x: 2,
                y: collarbone.out.y - 2
            };
            collarbone.in.cp1 = {
                x: collarbone.out.x - 5,
                y: collarbone.out.y
            };
            collarbone.in.cp2 = {
                x: collarbone.in.x + 1,
                y: collarbone.in.y + 2
            };
        }
        return [ex.collarbone.out, ex.collarbone.in];
    }
}


export class PectoralOutline extends MuscleOutline {
    constructor(...data) {
        super({
            loc        : "+torso",
            aboveParts : ["parts torso"],
            belowParts : ["parts chest", "decorativeParts chest"],
            layer      : Layer.FRONT,
            reflect    : true,
            muscleGroup: "upperMuscle",
        }, ...data);
    }

    calcDrawPoints(ex, ignore, calculate) {
        if (this.upperMuscle < 20) {
            return [];
        }
        if (calculate) {
            const bulk = this.upperMuscle - 20;
            let pecs = ex.pecs = {};
            pecs.outtop = {
                x: ex.armpit.x,
                y: ex.armpit.y + 4
            };
            pecs.outbot = {
                x: pecs.outtop.x - 3,
                y: pecs.outtop.y - 9
            };
            pecs.inbot = {
                x: 2,
                y: pecs.outbot.y + 0.5
            };
            pecs.intop = {
                x: 0.5,
                y: pecs.inbot.y + 1 + bulk * 0.3
            };

            pecs.outbot.cp1 = {
                x: pecs.outtop.x,
                y: pecs.outtop.y - 3
            };
            pecs.outbot.cp2 = {
                x: pecs.outbot.x + 1,
                y: pecs.outbot.y + 1
            };
            pecs.inbot.cp1 = {
                x: pecs.outbot.x - 0.5,
                y: pecs.outbot.y - 1
            };
            pecs.inbot.cp2 = {
                x: pecs.inbot.x + 0.5,
                y: pecs.inbot.y - 1
            };
            pecs.intop.cp1 = {
                x: pecs.inbot.x - bulk * 0.1,
                y: pecs.inbot.y + bulk * 0.05
            };
            pecs.intop.cp2 = {
                x: pecs.intop.x,
                y: pecs.intop.y - bulk * 0.1
            };
        }
        return [ex.pecs.outtop, ex.pecs.outbot, ex.pecs.inbot, ex.pecs.intop];
    }
}


export class AbdominalOutline extends MuscleOutline {
    constructor(...data) {
        super({
            loc        : "+torso",
            aboveParts : ["parts torso"],
            layer      : Layer.FRONT,
            reflect    : true,
            muscleGroup: "upperMuscle",
        }, ...data);
    }

    calcDrawPoints(ex, ignore, calculate) {
        if (this.upperMuscle < 15) {
            return [];
        }
        const bulk = this.upperMuscle - 20;
        if (calculate) {
            const length = (this.height - this.legLength) * 0.75;
            const abLength = length * 0.12;
            const abBulge = bulk * 0.05;

            let abs = ex.abs = {};

            // side details
            abs.top = {
                x: ex.waist.x - 3,
                y: ex.armpit.y - 7
            };
            abs.first = {
                x: abs.top.x - 1,
                y: abs.top.y - abLength
            };
            abs.first.cp1 = simpleQuadratic(abs.top, abs.first, 0.5, abBulge);
            abs.second = {
                x: abs.first.x - 0.4,
                y: abs.first.y - abLength
            };
            abs.second.cp1 = simpleQuadratic(abs.first, abs.second, 0.5, abBulge);
            abs.bot = {
                x: abs.second.x - 0.3,
                y: abs.second.y - abLength
            };
            abs.bot.cp1 = simpleQuadratic(abs.second, abs.bot, 0.5, abBulge);

            // inside
            if (bulk > 5) {

                abs.top.out = {
                    x: abs.top.x - 1.7,
                    y: abs.top.y
                };
                abs.top.in = {
                    x: 0.9,
                    y: abs.top.y + 0.5
                };
                abs.top.in.cp1 = simpleQuadratic(abs.top, abs.top.in, 0.5, -abBulge);

                abs.first.out = {
                    x: abs.first.x - 1.7,
                    y: abs.first.y
                };
                abs.first.in = {
                    x: 1.5,
                    y: abs.first.y
                };
                abs.first.in.cp1 = simpleQuadratic(abs.first, abs.first.in, 0.5, abBulge);

                abs.second.out = {
                    x: abs.second.x - 1.7,
                    y: abs.second.y
                };
                abs.second.in = {
                    x: 1.2,
                    y: abs.second.y
                };
                abs.second.in.cp1 = simpleQuadratic(abs.second, abs.second.in, 0.5, abBulge);

                abs.bot.in = {
                    x: 0.6,
                    y: abs.bot.y
                };
                abs.bot.in.cp1 = simpleQuadratic(abs.bot, abs.bot.in, 0.5, abBulge);

                // lats
                let lat = ex.lat;
                lat.top = {
                    x: lat.x - 1.5,
                    y: lat.y
                };
                lat.bot = {
                    x: abs.top.x + 1,
                    y: abs.top.y * 0.5 +
                       abs.first.y * 0.5
                };
                lat.bot.cp1 = simpleQuadratic(lat.top, lat.bot, 0.5, abBulge * 1.5);
            }
        }
        let points = [
            ex.abs.top,
            ex.abs.first,
            ex.abs.second,
            ex.abs.bot,
        ];
        if (bulk > 5) {
            points = [
                ...points,
                // inside abs
                breakPoint,
                ex.abs.top.out,
                ex.abs.top.in,
                breakPoint,
                ex.abs.first.out,
                ex.abs.first.in,
                breakPoint,
                ex.abs.second.out,
                ex.abs.second.in,
                // lats
                breakPoint,
                ex.lat.top,
                ex.lat.bot,
            ];
        }
        return points;
    }
}


export class QuadricepsOutline extends MuscleOutline {
    constructor(...data) {
        super({
            loc        : "+leg",
            aboveParts : ["parts leg"],
            belowParts : ["clothingParts leg", "clothingParts feet"],
            layer      : Layer.FRONT,
            muscleGroup: "lowerMuscle",
        }, ...data);
    }

    fill(ctx, ex) {
        return inheritFill.call(this, ctx, ex);
    }

    clipFill() {

    }

    calcDrawPoints(ex, ignore, calculate) {
        if (this.lowerMuscle < 20 || ex.hasOwnProperty("thigh") === false) {
            return [];
        }
        if (calculate) {
            const bulk = this.lowerMuscle - 10;
            let quads = ex.quads = {};
            quads.top = {
                x: ex.thigh.in.x * 0.1 + ex.thigh.out.x * 0.9,
                y: ex.thigh.out.y,
            };

            const sp = splitCurve(0.95, ex.thigh.out, ex.knee.out);
            quads.out = sp.right.p1;
            quads.out.cp1 = simpleQuadratic(quads.top, quads.out, 0.55, bulk * 0.25);

            quads.in = {
                x: ex.thigh.in.x * 0.65 + ex.thigh.out.x * 0.35,
                y: ex.thigh.in.y + 3.5
            };
            quads.bot = {
                x: ex.knee.out.x - 6 - bulk * 0.05,
                y: ex.knee.intop.y + 2
            };
            quads.bot.cp1 = simpleQuadratic(quads.in, quads.bot, 0.7, -bulk * 0.1);
        }
        return [ex.quads.top, ex.quads.out, breakPoint, ex.quads.in, ex.quads.bot];
    }
}

