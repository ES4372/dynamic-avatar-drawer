/*eslint no-unused-vars: ["error", {"varsIgnorePattern": "\$[^\b]*\$"}]*/
import {IMAGE_MAXSIZE, getPattern, getPatternBaseName, isPattern} from "./pattern";
import {Part} from "../parts/part";
import {Clothes} from "../clothes/clothing";

// Utility method to allow users to easily wrap their code in the revive wrapper.
if (!JSON.reviveWrapper) {
    Object.defineProperty(JSON, "reviveWrapper", {
        configurable: true,
        writable    : true,

        value: function (code, data) {
            if (typeof code !== "string") {
                throw new TypeError("JSON.reviveWrapper code parameter must be a string");
            }

            return ["(revive:eval)", [code, data]];
        }
    });
}

// Serialize data into a JSON-encoded string.
export function serialize(value, space) {
    return JSON.stringify(value, function (key, value) {
        // _owner often has self-reference and should not be serialized
        if (key === "_owner" || key === "shadingParts") {
            return undefined;
        }
        return value;
    }, space);
}

export function deserialize(text) {
    return JSON.parse(text, function (key, val) {
        let value = val;

        // Attempt to revive wrapped values.
        if (Array.isArray(value) && value.length === 2 && value[0] === "(revive:eval)") {
            const $ReviveData$ = value[1][1];
            value = eval(value[1][0]); // eslint-disable-line no-eval
        } else if (isPattern(value)) {
            // revive pattern object
            const {patternName, patternSize = IMAGE_MAXSIZE} = value[1];
            value = getPattern(getPatternBaseName(patternName), patternSize);
        }

        return value;
    });
}

function extendSerializability(globalClassIdentifier) {
    const classConstructor = eval(globalClassIdentifier);
    Object.defineProperties(classConstructor.prototype, {
        /*
         Returns a simple object encapsulating our own data properties.
         */
        _getData: {
            value: function () {
                const data = {};
                Object.keys(this).forEach(function (key) {

                    // need recursive deserialization
                    data[key] = this[key];
                }, this);

                return data;
            }
        },

        /*
         Clones values from the given object to our own data properties.

         Returns a self-reference which allows `toJSON()` to be simpler.
         */
        _setData: {
            value: function (data) {
                return new this.constructor(data);
            }
        },


        /*
         Allows the object to be properly cloned from passage to passage.
         */
        clone: {
            value: function () {
                return new classConstructor(this._getData);
            }
        },

        /*
         Allows the object to be properly restored from serializations.
         */
        toJSON: {
            value: function () {
                return JSON.reviveWrapper(
                    "(new " + globalClassIdentifier + "($ReviveData$))",
                    this._getData()
                );
            }
        }
    });
}

export function loadSerialization() {
    /*
     Merge the requisite properties onto instantiable classes (only).
     */
    const globalClassNames = [];
    const namespace = window.da;

    for (let className in namespace) {
        if (namespace.hasOwnProperty(className) === false) {
            continue;
        }
        const exportedClass = namespace[className];

        if (typeof exportedClass === "function") {
            // heuristic of constructors always having a capital letter...
            if (className.length > 1 &&
                className.charAt(0) === className.charAt(0).toUpperCase()) {
                globalClassNames.push(className);
            }
        }
    }

    // Add entries for the rest of DAD's instantiable classes here.
    globalClassNames.forEach(function (className) {
        extendSerializability("da." + className);
    });
}
