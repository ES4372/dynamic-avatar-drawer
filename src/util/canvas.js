/**
 * Canvas drawing layer order; higher value is further up
 * @memberof module:da
 * @readonly
 * @enum {number}
 */
export const Layer = Object.freeze({
    BASE              : 0,
    BACK              : 1,
    FRONT             : 2,
    SHADING_FRONT     : 3,
    ARMS              : 4,
    SHADING_ARMS      : 5,
    GENITALS          : 6,
    SHADING_GENITALS  : 7,
    BELOW_HAIR        : 8,
    SHADING_BELOW_HAIR: 9,
    HAIR              : 10,
    SHADING_HAIR      : 11,
    ABOVE_HAIR        : 12,
    EFFECTS           : 13,
    NUM_LAYERS        : 14
});

export const ShadingLayers = [
    Layer.SHADING_FRONT,
    Layer.SHADING_ARMS,
    Layer.SHADING_GENITALS,
    Layer.SHADING_BELOW_HAIR,
    Layer.SHADING_HAIR
];

/**
 * Minimal distance drawing on canvas to eliminate seams
 * @type {number}
 */
export const seamWidth = 0.0;

/**
 * Get access to a canvas group DOM element, creating it if it doesn't exist
 * @memberof module:da
 * @param {(string|HTMLElement)} groupname Id of the canvas group holder, or the DOM element
 * @param {object} styleOverride Object holding canvas style overrides
 * @returns {HTMLElement} The canvas group holder HTML DOM element
 */
export function getCanvasGroup(groupname, styleOverride) {
    let groupObj = groupname;
    if (typeof groupname === "string") {
        groupObj = document.getElementById(groupname);
    } else {
        groupname = groupname.id;
    }

    const styles = Object.assign(
        {
            width : "500",
            height: "800",
            parent: groupObj,
        },
        styleOverride);

    // size so surroundings can respect our boundaries
    groupObj.style.width = styles.width + "px";
    groupObj.style.height = styles.height + "px";
    groupObj.style.textAlign = "left";

    const group = [];
    // create various canvas layers with the same override
    for (let layer = 0; layer <= Layer.NUM_LAYERS; ++layer) {
        const canvasName = groupname + layer;
        let hideWorkingCanvas = null;
        // only display final canvas
        if (layer !== Layer.NUM_LAYERS) {
            hideWorkingCanvas = {"visibility": "hidden"};
        }
        group.push(getCanvas(
            canvasName,
            Object.assign({}, styles, {"z-index": layer}, hideWorkingCanvas)));
    }
    return groupObj;
}


/**  Get a canvas DOM element with id=canvasName, generating it if necessary
 styleOverride is the additional/overriding css style object to apply over
 defaults
 likely, you'd want to define its location:

 styles = {
     position:"absolute",
     top:"10px",
     left:"10px",
     parent: document.getElementById("canvas_holder"),
     }
 * @memberof module:da
 */
export function getCanvas(canvasName, styles) {
    // if given a canvas object, just return it
    if (typeof canvasName !== "string") {
        return canvasName;
    }

    let canvas = document.getElementById(canvasName);
    // create canvas
    if (!canvas) {
        if (styles.hasOwnProperty("parent") === false) {
            styles.parent = document.createElement("div");
            document.body.appendChild(styles.parent);
            styles.parent.style.width = styles.width + "px";
            styles.parent.style.height = styles.height + "px";
        }

        canvas = document.createElement("canvas");
        canvas.id = canvasName;
        // width and height have to be set on the DOM element rather than styled
        canvas.width = styles.width;
        canvas.height = styles.height;
        canvas.style.position = "absolute";

        // add the rest of the styling
        for (let s in styles) {
            if (styles.hasOwnProperty(s) === false) {
                continue;
            }
            if (s === "width" || s === "height") {
                continue;
            } else if (styles.hasOwnProperty(s)) {
                canvas.style[s] = styles[s];
            }
        }

        // ensure is first child of parent
        styles.parent.insertBefore(canvas, styles.parent.firstChild);
    }

    return canvas;
}

/**
 * Hide a canvas group from view (but not delete it)
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function hideCanvasGroup(groupName) {
    const group = getCanvasHandle(groupName);
    group.style.display = "none";
}

/**
 * Show a canvas group from view
 * @memberof module:da
 * @param {(string|HTMLElement)} groupName Either the id of the group element or the element itself
 */
export function showCanvasGroup(groupName) {
    const group = getCanvasHandle(groupName);
    group.style.display = "block";
}

export function getCanvasHandle(canvasName) {
    if (typeof canvasName === "string") {
        return document.getElementById(canvasName);
    } else {
        return canvasName;  // assume canvas element passed in
    }

}
