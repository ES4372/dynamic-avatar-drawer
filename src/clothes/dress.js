import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {connectEndPoints, coverNipplesIfHaveNoBreasts} from "../draw/draw";
import {Layer} from "../util/canvas";
//import {setStrokeAndFill} from "../util/draw";
import {
    extractPoint,
	drawPoints, 
	splitCurve,
	breakPoint,
	clone,
	//none,
	adjust,
	reflect,
	interpolateCurve,
} from "drawpoint";
import {
	getLimbPoints,
	getLimbPointsAbovePoint,
	straightenCurve,
	findBetween,
	lineLineIntersection,
	lineCubicIntersection,
	pointLineIntersection,
	getLacingPoints,
} from "../util/auxiliary";
//test

export class DressBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {
			cleavageBot,cleavageTop
		} = calcDressBase.call(this, ex);
		
        // clip drawing to inside the dress
        ctx.beginPath();
        drawPoints(ctx,{x:0,y:0},cleavageBot,cleavageTop,{x:100,y:250},{x:100,y:0});
        ctx.clip();

        //nipples with no breasts
        if (coverNipplesIfHaveNoBreasts(ex, ctx, this)) {
            return;
        }

        const top = adjust(ex.breast.top, 0.1, 0.2);
        const tip = adjust(ex.breast.tip, 0.1, 0);
        const bot = adjust(ex.breast.bot, 0, -0.1);
        const inner = adjust(ex.breast.in, -0.4, 0);
        const cleavage = adjust(ex.breast.cleavage, -0.1, 0);
        const topAgain = adjust(connectEndPoints(cleavage, top), 0, 0.2);

        ctx.beginPath();
        drawPoints(ctx, top,tip,bot,inner, cleavage, topAgain);
        ctx.fill();

        //line showing conture of breasts
        ctx.beginPath();
        drawPoints(ctx, top, tip, bot);
        ctx.stroke();

        //repair cleavage line
        ctx.beginPath();
        drawPoints(ctx,cleavageBot,cleavageTop);
        ctx.stroke();


    }
}

export class DressBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg","decorativeParts leg"], 
			aboveSameLayerParts: ["groin", "leg"], 
			
        }, {
            cleavageOpeness: 0.3,
			cleavageCoverage: 0.3,
			sideLoose: 0,
			legCoverage: 0.4,
			legLoose: 0,
			curveCleavageX:0,
			curveCleavageY:0,
			bustle: false,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

		const {
			cleavageBot,cleavageTop,neck,collarbone,armpit,waist,hip,outerPoints,shoulder,pit,bottom,
		} = calcDressBase.call(this, ex);
				
        Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			shoulder, 
			pit,  
			...outerPoints, 
			bottom
		);
		ctx.fill();

		
		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop, 
			neck, 
			shoulder, 
			breakPoint, 
			pit,   
			...outerPoints,
			bottom
		);
		ctx.stroke();

    }
}

export function calcDressCleavage(ex,bottom){
	//cleavage lowest point
	let cleavageBot;
	let cleavageTop;
	let neck;
	{
		let sp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		cleavageBot = {x:-0.1, y: sp.left.p2.y};
	}

	//cleavage highest point
	if(this.cleavageOpeness<0){//turtleneck
		let sp = splitCurve(1+this.cleavageOpeness,ex.neck.top,ex.neck.cusp);
		cleavageTop = extractPoint(sp.left.p2);
		neck = clone(ex.neck.cusp);
	}else if(this.cleavageOpeness>1){ //even to shoulders, messy wip
		let sp = splitCurve(this.cleavageOpeness-1,ex.collarbone, ex.shoulder);
		cleavageTop=extractPoint(sp.left.p2);			
	}else{
		let sp = splitCurve(this.cleavageOpeness,ex.neck.cusp, ex.collarbone);
		cleavageTop = extractPoint(sp.left.p2);
	}
	
	//check if cleavage goes bellow the bottom of the garment, fixes it if so
	if(!this.lacing&&bottom){
		if(cleavageBot.y<bottom.y)cleavageBot.y=bottom.y+3;
	}
	
	//and calculates the cleavage curve
	let sp = splitCurve(0.5,cleavageBot,cleavageTop); //cleavage curve
	cleavageTop.cp1 = {
		x: sp.left.p2.x+this.curveCleavageX,
		y: sp.left.p2.y+this.curveCleavageY,
	};
	
	return {
		cleavageBot: cleavageBot,
		cleavageTop: cleavageTop,
		neck: neck,
	};
}

function calcDressBase(ex){
	//arm
	let shoulder = clone(ex.collarbone);
	let pit = clone(ex.armpit);
	
	//waist
	let collarbone = clone(ex.collarbone);
	let armpit = clone(ex.armpit);
	let lat = clone(ex.lat);
	let hip = adjust(ex.hip, 0, 0);
	let waist = adjust(ex.waist, 0, 0); //adjust(ex.waist,(this.thickness * 0.8), 0); //last remnant of sweater
	
	//to have dress loose around the waist
	{
		let top = armpit;
		if(lat)top = lat;
		let mid = lineLineIntersection(top,hip,{x:0,y:waist.y},{x:100,y:waist.y});
		if(mid.x>waist.x){
			waist.x = findBetween(waist.x, mid.x,this.sideLoose);
			straightenCurve(armpit,waist,this.sideLoose);
			straightenCurve(waist,hip,this.sideLoose);
		}/*else{
			hip = extractPoint(hip);
		}*/
	}
	//if(waist.x<armpit.x)waist.x = findBetween(waist.x, findBetween(armpit.x,hip.x,0.5),this.sideLoose);

	
	
	//lower waist and legs
	let outerPoints;
	if(this.legCoverage<0){
		outerPoints = getLimbPoints(armpit,hip,1+this.legCoverage,armpit,lat,waist,hip);
	}else if(this.legCoverage==0){
		outerPoints = [armpit,lat,waist,hip];
	}else{
		let waistPoints = [armpit,lat,waist];
		let legPoints;
		
		if(this.legLoose>0){
			legPoints = getLimbPoints(hip,ex.ankle.out,this.legCoverage,hip,ex.thigh.out);
			let totalLegLength = hip.y-( (hip.y-ex.ankle.out.y)*this.legCoverage );
			if (totalLegLength<ex.groin.y){ 
				legPoints = [];
				legPoints[legPoints.length] = clone(hip);
				legPoints[legPoints.length] = clone(ex.thigh.out);
				
				if(this.bustle)legPoints[legPoints.length] = extractPoint(ex.thigh.out);
				
				legPoints[legPoints.length-1].y = totalLegLength;
				legPoints[legPoints.length-1].x += this.legLoose*30*this.legCoverage;
				
			} 
		}else{
			legPoints = getLimbPoints(hip,ex.ankle.out,this.legCoverage,hip,ex.thigh.out,ex.knee.out,ex.calf.out,ex.ankle.out);
		}
		
		outerPoints = waistPoints.concat(legPoints);
	}
	
	//bottom
	let bottom = {
			y:outerPoints[outerPoints.length-1].y,
			x:-0.1,
		};
	
	//cleavage
	const {
		cleavageBot,cleavageTop,neck
	} = calcDressCleavage.call(this,ex,bottom);
	
	//bottom curve
	outerPoints[outerPoints.length-1].y += 2;
	bottom.y += -2;
	bottom.cp1 = {
		x: bottom.x * 0.5 + outerPoints[outerPoints.length-1].x * 0.5,
		y: bottom.y
	};
	
	return {
			cleavageBot: cleavageBot,
			cleavageTop: cleavageTop,
			neck:neck,
			collarbone: collarbone,
			armpit:armpit,
			waist: waist,
			hip: hip, 
			
			outerPoints:outerPoints,
			
			shoulder:shoulder,
			pit:pit,
			 
			bottom: bottom
		};
	
	
}


export class DetachedSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            shoulderCoverage: 0.5,
			
			armCoverage: 0.5,
			armLoose: 0,
			}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(this.armCoverage<=0)return;
		
        let {
           outerArmPoints,
		   innerArmPoints
        } = calcSuperSleeve.call(this, ex);
		
		Clothes.simpleStrokeFill(ctx, ex, this);

		if(this.shoulderCoverage>=1){
			const con_collarbone = adjust(ex.collarbone,-0.5,0);
			const con_armpit = adjust(ex.armpit,-0.5,0);ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints, 
				...innerArmPoints,
				connectEndPoints(con_armpit,con_collarbone)
			);
			ctx.fill();
		
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints, 
				...innerArmPoints
			);
			ctx.stroke();
		}else{
		
			let temp = splitCurve(1-this.shoulderCoverage,ex.collarbone,ex.shoulder);
			
			//if(ex.deltoids)temp = splitCurve(1-this.shoulderCoverage,ex.collarbone,ex.deltoids)
			//asap - doesn't work with muscles
			
			outerArmPoints[0]=extractPoint(temp.left.p2);
			outerArmPoints[1]=extractPoint(temp.left.p2);
			outerArmPoints[2]=extractPoint(outerArmPoints[2]);
			
			ctx.beginPath();
			drawPoints(ctx, 
				...outerArmPoints, 
				...innerArmPoints,
				outerArmPoints[0]
			);
			ctx.fill();
			ctx.stroke();
		}

    }
}

export class SuperSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, {
            armCoverage: 0.5,
			armLoose: 0,
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(this.armCoverage<=0)return;
		
        const {
           outerArmPoints,
		   innerArmPoints
        } = calcSuperSleeve.call(this, ex);
		
		const con_collarbone = adjust(ex.collarbone,-0.5,0);
		const con_armpit = adjust(ex.armpit,-0.5,0);
				
        Clothes.simpleStrokeFill(ctx, ex, this);

		
		ctx.beginPath();
		drawPoints(ctx, 
			...outerArmPoints, 
			...innerArmPoints,
			connectEndPoints(con_armpit,con_collarbone)
		);
		ctx.fill();

	
		ctx.beginPath();
		drawPoints(ctx, 
			...outerArmPoints, 
			...innerArmPoints
		);
		ctx.stroke();
    }
}

function calcSuperSleeve(ex) {
	let outerArmPoints = [];
	let innerArmPoints = [];
	
	//loose //
	if (this.armLoose>0){
		let innerEndPoint = adjust(ex.wrist.in,0,0);
		innerEndPoint.y = ex.hand.tip.y;

		let shoulder = adjust(ex.shoulder,0.75*(5*this.armLoose),0);
		let deltoid = adjust(ex.deltoids,1*(5*this.armLoose),0);
		let knuckle = adjust(ex.hand.knuckle,1.5*(5*this.armLoose),0);
		//meh with muscles and loose shirt
		
		let tip=adjust(ex.hand.tip,1*(5*this.armLoose),0);
		
		outerArmPoints = getLimbPoints(ex.collarbone,ex.hand.palm,this.armCoverage,ex.collarbone,deltoid,shoulder,knuckle,tip);
 
		innerArmPoints = getLimbPoints(ex.collarbone,ex.hand.palm,this.armCoverage,ex.armpit,innerEndPoint);
		innerArmPoints.reverse();
		if(typeof innerArmPoints[0] !== "undefined"){innerArmPoints[0].x -=1*(5*this.armLoose)} 
		if(typeof innerArmPoints[1] !== "undefined"){innerArmPoints[1].x -=1*(5*this.armLoose)}
	//fitting
	}else{
		outerArmPoints = getLimbPoints(ex.collarbone,ex.hand.palm,this.armCoverage,ex.collarbone,ex.deltoids,ex.shoulder,ex.elbow.out,ex.wrist.out,ex.hand.knuckle,ex.hand.fist,ex.hand.tip);
		
		//this will turn the sleeve into glove
		if(outerArmPoints[outerArmPoints.length-1].y<=ex.thumb.tip.y){ 
			outerArmPoints[outerArmPoints.length]=ex.hand.palm;
			innerArmPoints = [ex.armpit,ex.elbow.in,ex.wrist.in,ex.thumb.out,ex.thumb.tip];
			innerArmPoints.reverse();
		}else{
			innerArmPoints = getLimbPointsAbovePoint(outerArmPoints[outerArmPoints.length-1],true,ex.armpit,ex.elbow.in,ex.wrist.in,ex.thumb.out,ex.thumb.tip);
		}
	}
		
	if(this.armCoverage<1){ //to have the bottom of the sleeve straight, not desirable if glove
		innerArmPoints[0] = extractPoint(innerArmPoints[0]);
	}
	
	return {
		outerArmPoints:outerArmPoints,
		innerArmPoints:innerArmPoints
	};
}
	
	
	
export class LacingPart extends ClothingPart {
    constructor(...data) {
        super({
            layer: Layer.GENITALS,
			loc: "chest",
			reflect: false,
			aboveParts: ["parts chest", "decorativeParts chest"]
			
        }, {
			lacing:false,
			crosses:0
		}, ...data);
    }

    renderClothingPoints(ex, ctx) {
		if(!this.lacing)return;
		
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		const {
			cleavageBot,cleavageTop
		} = calcDressCleavage.call(this,ex);
		
		let crosses=this.crosses; //crosses <0 = automatic number 
		if(crosses<=0)crosses = this.cleavageCoverage*13;
		
		let top = reflect(cleavageTop);
		top.cp1 = reflect(cleavageTop.cp1);
		top.cp2 = reflect(cleavageTop.cp2);
		
		let points = getLacingPoints(cleavageBot,cleavageTop,reflect(cleavageBot),top,crosses,0);
	
		ctx.lineWidth = 1;
		ctx.beginPath();
		drawPoints(ctx, ...points.inner, breakPoint, ...points.outer);
		ctx.stroke();
		
	}
}


/**
 * Base Clothing classes
 */
export class Dress extends Clothing {
    constructor(...data) {
        super({
            clothingLayer  : Clothes.Layer.MID,
            armCoverage: 0.5,
			armLoose: 0,
			thickness: 1,
        }, ...data);
    }
}
	

export class SuperDress extends Dress {
    constructor(...data) {
        super({
			
        }, ...data);
    }

    stroke() {
        return "hsla(335, 80%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },{
                side: null,
                Part: DressBasePart
            },
             { 
                side: null,  
                Part: DressBreastPart
            },
		
            {
                side: Part.LEFT,
                Part: SuperSleevePart
            },
            {
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
        ];
    }
}