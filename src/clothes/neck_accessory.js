import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Layer} from "../util/canvas";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    adjust,
    clamp,
    rotatePoints,
    rad,
    reflect,
} from "drawpoint";

function calcNecktieLoop(params) {
    // order will be rightBot->rightTop->leftTop->leftBot->rightBot
    const rightTop = extractPoint(params.collarTop);
    const rightBot = extractPoint(params.collarBot);
    const leftTop = extractPoint(rightTop);
    leftTop.x = -leftTop.x;
    const leftBot = extractPoint(rightBot);
    leftBot.x = -leftBot.x;

    rightTop.cp1 = simpleQuadratic(rightBot, rightTop, 0.5, 1);
    // curve between left and right
    leftTop.cp1 = params.center;
    leftBot.cp1 = simpleQuadratic(leftTop, leftBot, 0.5, 1);
    // curve between left and right
    rightBot.cp1 = adjust(params.center, 0, rightBot.y - rightTop.y);

    return {
        rightBot,
        rightTop,
        leftTop,
        leftBot
    };
}

/**
 * ClothingPart drawn classes/components
 */
export class NecktiePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "+neck",
            reflect            : false,
            aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],
            belowSameLayerParts: ["torso"],
        }, {
            /**
             * Positional offset of the top of the knot relative to the center of the neck
             */
            offsetWidth    : 0,
            offsetHeight   : -10,
            knotWidth      : 5,
            knotHeight     : 5,
            loopWidth      : 3,
            // top part of tongue's width
            tongueTopWidth : 4,
            // widest part (at the bottom) of the tongue's width
            tongueBotWidth : 6,
            // total length of tongue
            tongueHeight   : 15,
            // from the bottom, where the widest part occurs
            tongueBotHeight: 3,
            // rotation of the tip of the tongue
            tongueRotation : 0,
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {

        // center of the neck top (top of knot)
        const center = {
            x: this.offsetWidth,
            y: ex.neck.cusp.y + this.offsetHeight
        };

        // knot
        const knot = {};
        knot.topLeft = {
            x: center.x - this.knotWidth / 2,
            y: center.y
        };
        knot.topRight = {
            x: center.x + this.knotWidth / 2,
            y: center.y
        };
        knot.left = {
            x: knot.topLeft.x,
            y: center.y - this.loopWidth / 2
        };
        knot.right = {
            x: knot.topRight.x,
            y: knot.left.y
        };
        knot.botLeft = {
            x: center.x - this.tongueTopWidth / 2,
            y: center.y - this.knotHeight
        };
        knot.botRight = {
            x: center.x + this.tongueTopWidth / 2,
            y: knot.botLeft.y
        };

        // neck loop
        const sp = splitCurve(clamp(this.loopWidth * 0.035, 0, 1), ex.trapezius,
            ex.collarbone
        );
        const botRight = extractPoint(sp.left.p2);
        const topRight = sp.left.p1;
        topRight.cp1 = sp.left.p2.cp2;
        topRight.cp2 = sp.left.p2.cp1;

        const topLeft = reflect(topRight);
        const botLeft = reflect(botRight);
        topLeft.cp1 = reflect(topRight.cp1);
        topLeft.cp2 = reflect(topRight.cp2);

        // tongue
        const tongue = {};
        tongue.bot = {
            x: center.x,
            y: knot.botRight.y - this.tongueHeight
        };
        tongue.left = {
            x: center.x - this.tongueBotWidth / 2,
            y: tongue.bot.y + this.tongueBotHeight,
        };
        tongue.right = {
            x: center.x + this.tongueBotWidth / 2,
            y: tongue.left.y,
        };
        // rotate just the tongue points and not the control points
        rotatePoints(center,
            rad(this.tongueRotation),
            tongue.bot,
            tongue.left,
            tongue.right);
        // TODO set control points that won't be rotated

        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            knot.topLeft,
            center,
            knot.topRight,
            knot.right,
            knot.botRight,
            knot.botLeft,
            knot.left,
            knot.topLeft
        );

        const deflection = 1.5;
        // top left down to knot
        knot.topLeft.cp1 = simpleQuadratic(topLeft, knot.topLeft, 0.5, -deflection);
        botLeft.cp1 = simpleQuadratic(knot.left, botLeft, 0.5, deflection);
        drawPoints(ctx,
            botLeft, topLeft, knot.topLeft, knot.left, botLeft
        );

        // top right down to knot
        knot.topRight.cp1 = simpleQuadratic(topRight, knot.topRight, 0.5, deflection);
        botRight.cp1 = simpleQuadratic(knot.right, botRight, 0.5, -deflection);
        drawPoints(ctx,
            botRight, topRight, knot.topRight, knot.right, botRight
        );

        // tongue
        drawPoints(ctx,
            knot.botLeft, tongue.left, tongue.bot, tongue.right, knot.botRight
        );
        ctx.fill();
    }
}


/**
 * Base Clothing classes
 */
export class NeckAccessory extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.MID,
        }, ...data);
    }
}


/**
 * Concrete Clothing classes
 */
export class NeckTie extends NeckAccessory {
    constructor(...data) {
        super(...data);
    }

    fill() {
        return "#333";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: NecktiePart,
            }
        ];
    }
}
