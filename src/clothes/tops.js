import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
//import {connectEndPoints} from "../draw/draw";
import {Layer} from "../util/canvas";
//import {setStrokeAndFill} from "../util/draw";
import {
  extractPoint,
	drawPoints, 
	splitCurve,
	breakPoint,
	clone,
	adjust,
} from "drawpoint";
import {
	straightenCurve,
	findBetween,
	lineLineIntersection,
	//getLacingPoints,
} from "../util/auxiliary";

import {DressBreastPart,SuperSleevePart,DetachedSleevePart,calcDressCleavage,LacingPart} from "./dress";


//basically calcDressBase but only goes to waist
function calcTee(ex){
	//arm
	let shoulder = clone(ex.collarbone);
	
	//waist points
	const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
	
	//cleavage
	const {
		cleavageBot,cleavageTop,neck
	} = calcDressCleavage.call(this,ex,bottom);
	  
	return {
			cleavageBot: cleavageBot,
			cleavageTop: cleavageTop,
			neck:neck,
			shoulder:shoulder,
			
			armpit:armpit,
			lat:lat,
			waist: waist,
			hip: hip, 
			out:out, 
			bottom: bottom
		};
}


export class TeePart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"],*/
			
        }, {

		}, ...data);
    }

    renderClothingPoints(ex, ctx) {

		const {
			cleavageBot,cleavageTop,neck,shoulder,armpit,waist,hip,out,bottom,
		} = calcTee.call(this, ex);
		
		 Clothes.simpleStrokeFill(ctx, ex, this);

		ctx.beginPath();
		drawPoints(ctx, 
			cleavageBot,  
			cleavageTop,  
			neck, 
			shoulder, 
			armpit,  
			waist,
			hip,
			out,
			bottom
			
		);
		ctx.fill();
		ctx.stroke();

    }
}


export class HalterTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		//check and recalculate cleavage if deep beyond limit
		function checkCleavage(context,bottom){
			if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
						
			sp = splitCurve(0.5,cleavage,topIn);
			topIn.cp1 = {
				x: sp.left.p2.x+context.curveCleavageX,
				y: sp.left.p2.y+context.curveCleavageY,
			};
		
		}
		
		//CLEAVAGE
		let sp = splitCurve(this.cleavageCoverage,ex.neck.cusp, ex.groin);
		let cleavage = {
			x : -0.1,
			y : sp.left.p2.y
		};
		
		if(this.outerNeckCoverage<this.innerNeckCoverage)this.outerNeckCoverage=this.innerNeckCoverage;
		if(this.innerNeckCoverage>this.outerNeckCoverage)this.innerNeckCoverage=this.outerNeckCoverage;
		
		let cusp = ex.neck.cusp;
		if(ex.trapezius)cusp=ex.trapezius;
		sp = splitCurve(this.outerNeckCoverage,cusp, ex.collarbone);
		let topOut = sp.left.p2;
				
		if(this.innerNeckCoverage<0){
			sp = splitCurve(1+this.innerNeckCoverage, ex.neck.top, cusp);
		}else{
			sp = splitCurve(this.innerNeckCoverage, cusp, ex.collarbone);
		}
		let topIn = extractPoint(sp.left.p2);
				
		
		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			let armpit = adjust(ex.armpit,0,0);
			let bottom = {
				x: -0.1,
				y: armpit.y-1
			};		
			//if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
			checkCleavage(this,bottom);
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit),
				bottom
			);
			ctx.fill();
			
			ctx.beginPath();
			drawPoints(ctx,
				cleavage,
				topIn,
				topOut,
				extractPoint(armpit)
			);	
			ctx.stroke();
			

			ctx.save();
			ctx.lineWidth = 3;
			ctx.strokeStyle = ctx.fillStyle; 
			ctx.beginPath();
			drawPoints(ctx, breakPoint, ex.chest.nipples);
			ctx.stroke();
			ctx.restore();
			
			return;
		}
		
		//BREAST		
		//calculate points - I really dunno how, but it do 
		//const top = adjust(ex.breast.top, 0.1, 0.2);
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		let bottom = {
			x: -0.1,
			y: bot.y
		};
		
		//if(cleavage.y<bottom.y)cleavage.y=bottom.y+3;
		checkCleavage(this,bottom);
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			cleavage,
			topIn,
			topOut,
			//top,
			tip,
			bot
		);
		ctx.stroke();
		
    }
}


export class TubeTopBreastPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.GENITALS,
            loc                : "chest",
            reflect            : true,
           aboveParts: ["parts chest", "decorativeParts chest"]
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);

		//NO BREASTS
		if (ex.hasOwnProperty("breast") === false) {
			
			ctx.save();
			ctx.lineWidth = 3;
			ctx.strokeStyle = ctx.fillStyle;
				 
			
			ctx.beginPath();
			drawPoints(ctx, breakPoint, ex.chest.nipples);
			ctx.stroke();
			ctx.restore();
			 
			return;
		}
		
		
		let temp = splitCurve(this.chestCoverage,ex.breast.top,ex.breast.tip);
		/*
			//this is just for transformations: 
			let mid1;
			let mid2;
			if(this.chestCoverage<0){
				temp = splitCurve(Math.abs(this.chestCoverage),ex.breast.top,ex.collarbone);
				let mid1 = ex.breast.top;
			}else if(this.chestCoverage<-1){
				splitCurve(Math.abs(this.chestCoverage+1),ex.collarbone,ex.neck.cusp);
				let mid1 = ex.collarbone;
				let mid2 = ex.breast.top;
			}
		*/
		const topOut = extractPoint(temp.left.p2);
		const topIn = {
			x : -0.1,
			y : topOut.y
		};
		
		//calculate points - I really dunno how, but it do 
		//const top = adjust(ex.breast.top, 0.1, 0.2);
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		
		const bottom = {
			x: -0.1,
			y: bot.y
		};
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			topOut,
			//mid1,
			//mid2,
			tip,
			bot,
			bottom
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx,
			topIn,
			topOut,
			tip,
			bot
		);
		ctx.stroke();
		
    }
}


export class TopChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer              : Layer.FRONT,
            loc                : "torso",
            reflect            : true,
           /* aboveParts         : ["parts neck", "parts torso", "decorativeParts torso"],*/
            aboveParts			: ["parts neck", "parts torso", "decorativeParts torso", "parts leg"],
			/*aboveSameLayerParts: ["groin", "parts leg"]*/
        }, {
            
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
		Clothes.simpleStrokeFill(ctx, ex, this);
		
		if(this.waistCoverage>=2)return;
		
		//waist points
		const {armpit,lat,waist,hip,out,bottom} = calcTopBody.call(this, ex);
		
		let top = {};
		if(ex.breast){
			top = {
				x: -0.1,
				y: ex.breast.in.y
			};
		}else{
			top = {
				x: -0.1,
				y: ex.armpit.y
			};
		}
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit,  
			lat,
			waist,
			hip,
			out,
			bottom,
			top,
			extractPoint(armpit)
		);
		ctx.fill();
		
		ctx.beginPath();
		drawPoints(ctx, 
			armpit, 
			lat,
			waist,
			hip,
			out,
			bottom
		);
		ctx.stroke();
		
    }
}


function calcTopBody(ex){
	let armpit = clone(ex.armpit);
	let lat = clone(ex.lat);
	//looseness of waist points 
	let hip = adjust(ex.hip, 0, 0);
	let waist = adjust(ex.waist,(this.thickness * 0.8), 0); //last remnant of sweater
	
	
	//to have top loose around the waist (the same function as for dress)
	{
		let top = armpit;
		if(lat)top = lat;
		let mid = lineLineIntersection(top,hip,{x:0,y:waist.y},{x:100,y:waist.y});
		if(mid.x>waist.x){
			waist.x = findBetween(waist.x, mid.x,this.sideLoose);
			straightenCurve(armpit,waist,this.sideLoose);
			straightenCurve(waist,hip,this.sideLoose);
		};
	}
	
	let out;
	//coverage of waist
	if (this.waistCoverage > 1){
		let sp = splitCurve(1-(this.waistCoverage-1),ex.armpit,waist); //THIS????????
		waist = void 0;
		hip = void 0;
		out = sp.left.p2;
	}else if (this.waistCoverage >= 0){
		let sp = splitCurve(1-this.waistCoverage,waist,hip);
		//waist =  adjust(ex.hip, 0, 0);
		hip = void 0;
		out = sp.left.p2;
    }else{
        let sp = splitCurve(Math.abs(this.waistCoverage),hip,ex.thigh.out);
		out = sp.left.p2;
    }
	
	//bottom
	let bottom = {
			y:out.y-3,
			x:-0.1,
		};
	
	bottom.cp1 = {
		x: bottom.x * 0.5 + out.x * 0.5,
		y: bottom.y
	};
	
	return {
		armpit:armpit,
		lat:lat,
		waist: waist,
		hip: hip, 
		out:out, 
		bottom: bottom
	};
}

/**
 * Base Clothing classes
 */
 
export class Top extends Clothing {
    constructor(...data) {
		super({
			clothingLayer  : Clothes.Layer.MID,
			thickness: 0.6,
		}, ...data);
    }
	
	stroke() {
        return "hsla(335, 800%, 30%, 1)";
    }

    fill() {
        return "hsla(335, 100%, 42%, 1)";
    }
}
	
 
export class Tee extends Top {
    constructor(...data) {
        super({
			cleavageOpeness: 0.3,
			cleavageCoverage: 0.16,
			sideLoose: 0,
			waistCoverage: 0,
			curveCleavageX:0,
			curveCleavageY:0,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{ 
                side: null,  
                Part: LacingPart
            },
			{
                side: null,
                Part: TeePart
            },{ 
                side: null,  
                Part: DressBreastPart
            },{
                side: Part.LEFT,
                Part: SuperSleevePart
            },{
                side: Part.RIGHT,
                Part: SuperSleevePart
            },
			
        ];
    }
}
	

export class HalterTop extends Top {
    constructor(...data) {
        super({
			cleavageCoverage: 0.3,
			outerNeckCoverage: 0.35,
			innerNeckCoverage: 0.15,
			curveCleavageX: 9,
			curveCleavageY: -9,
			waistCoverage: 0.66,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
          {
                side: null,
                Part: HalterTopBreastPart
            },{ 
                side: null,  
                Part: TopChestPart
            },

        ];
    }
}	


export class TubeTop extends Top {
    constructor(...data) {
        super({
			chestCoverage: 0.3,
			waistCoverage: 0.3,
			sideLoose: 0,
        }, ...data);
    }

    get partPrototypes() {
        return [
			{
                side: null,
                Part: TubeTopBreastPart
            },{ 
                side: null,  
                Part: TopChestPart
            },

        ];
    }
}
