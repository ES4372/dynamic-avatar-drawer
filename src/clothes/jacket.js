import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    none,
    splitCurve,
    adjust,
    clamp,
    continueCurve,
    interpolateCurve,
    transformCurve,
} from "drawpoint";

export class JacketBaseShading extends ShadingPart {
    constructor(...data) {
        super({
            loc       : "+torso",
            layer     : Layer.GENITALS,
            forcedSide: Part.LEFT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const {top, mid, bot} = calcJacket.call(this, ex);
        const chestDisplay = 1 - this.chestCoverage;
        let inBot = extractPoint(mid);
        inBot.cp1 = bot.cp1;
        inBot = adjust(inBot, -chestDisplay * 3, chestDisplay * 3);

        top.cp1 = adjust(mid.cp2, -chestDisplay * 0, -chestDisplay);
        top.cp2 = adjust(mid.cp1, -chestDisplay * 6, -chestDisplay * 2);
        return [top, mid, bot, inBot, inBot, top];
    }
}


export class JacketBasePart extends ClothingPart {
    constructor(...data) {
        super({
            layer       : Layer.GENITALS,
            loc         : "torso",
            reflect     : true,
            aboveParts  : ["parts neck", "parts chest", "decorativeParts chest"],
            belowParts  : ["head"],
            shadingParts: [JacketBaseShading],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const {collarbone, top, mid, bot, outBot, outMid, breastBot, breastTip, outTop} = calcJacket.call(
            this,
            ex);


        Clothes.simpleStrokeFill(ctx, ex, this);

        // base
        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            top,
            mid,
            bot,
            outBot,
            outMid,
            breastBot,
            breastTip,
            outTop
        );
        ctx.fill();
        ctx.stroke();


        // lining
        setStrokeAndFill(ctx,
            {
                stroke: this.liningPattern,
                fill  : none
            },
            ex);
        ctx.lineWidth = this.liningWidth;
        ctx.clip();
        ctx.beginPath();
        drawPoints(ctx,
            top, mid, bot, outBot
        );
        ctx.stroke();
    }
}


/**
 * Calculate the drawpoints for a jacket
 * @this {stomachCoverage: Number, chestCoverage: Number}
 * @param ex
 * @returns {{collarbone: ({x, y}|*), top, mid: *, bot: {x: number, y}, outBot: Object, outMid: *, breastBot: *, breastTip: *, outTop: *}}
 */
export function calcJacket(ex) {
    let sp = splitCurve(0.2, ex.trapezius, ex.collarbone);
    const top = sp.left.p2;
    const collarbone = extractPoint(ex.collarbone);
    let mid, outMid;

    const chestDisplay = 1 - this.chestCoverage;

    sp = splitCurve(this.stomachCoverage, ex.waist, ex.hip);
    const bot = {
        x: sp.left.p2.x * (0.2 + clamp(chestDisplay, 0, 0.8)),
        y: sp.left.p2.y
    };
    const outBot = adjust(sp.left.p2, 1, -1);

    // accommodate breasts
    let outTop, breastTip, breastBot;
    const breast = ex.breast;
    if (breast) {
        mid = {
            x: ex.waist.x * clamp(chestDisplay * 0.8, 0, 1),
            y: bot.y * 0.5 + breast.bot.y * 0.5
        };
        mid.cp1 = {
            x: breast.center.x * clamp(0.6 - this.chestCoverage * 0.7, 0, 1) -
               chestDisplay * 2,
            y: breast.top.y
        };
        mid.cp2 = {
            x: breast.tip.x * clamp(1.2 - this.chestCoverage * 0.85, 0, 1),
            y: breast.tip.y
        };
        bot.cp1 = continueCurve(top, mid, clamp(0.2 + chestDisplay * 0.5, 0, 1));

        // outTop = adjust(extractPoint(breast.top), 1.5, 2);
        outTop = extractPoint(breast.top);

        breastTip = adjust(extractPoint(breast.tip), 0.3, 0);
        const breastProminance = breast.tip.x - ex.waist.x;
        sp = splitCurve(clamp(breastProminance * 0.06 - 0.2, 0, this.stomachCoverage),
            ex.waist,
            outBot
        );
        outMid = sp.left.p2;
        // where outMid would be if cling = 1
        const outMidTight = outMid;
        outMidTight.x += 0.1;
        outMid.cp1 = sp.right.p2.cp2;
        outMid.cp2 = sp.right.p2.cp1;
        outMid = adjust(outMid, 1, 0);
        if (breastProminance > 2) {
            // outMid = null;
            sp = splitCurve(clamp(breastProminance * 0.065, 0, 1),
                breast.tip,
                breast.bot
            );
            breastBot =
                adjust(sp.left.p2,
                    breastProminance * 0.05,
                    -breastProminance * 0.05 - 1);
            breastBot.cp1 = continueCurve(outBot, outMid, 1);
            breastTip.cp1 = sp.left.p2.cp2;
            breastTip.cp2 = sp.left.p2.cp1;
            breastTip.cp2.y -= clamp(3 - breastProminance * 0.2, 0, 5);
        }

        outTop.cp1 = breast.tip.cp2;
        outTop.cp2 = breast.tip.cp1;

        // adjust based on how much the clothes clings to body
        // cling = 0 -> smooth flow over breasts
        // cling = 1 -> tight against body
        outMid = adjust(outMid, (outMidTight.x - outMid.x) * this.cling, 0);
        let startPoint, endPoint;
        // depending on how high we are, we must project onto different curves
        if (breastBot.y > ex.waist.y) {
            startPoint = ex.lat;
            endPoint = ex.waist;
        } else {
            startPoint = ex.waist;
            endPoint = ex.hip;
        }
        const breastBotTight = interpolateCurve(startPoint,
            endPoint,
            {
                x: null,
                y: breastBot.y
            });
        sp = splitCurve(breastBotTight.t, startPoint, endPoint);
        // reverse direction
        breastBotTight.cp1 = sp.right.p2.cp2;
        breastBotTight.cp2 = sp.right.p2.cp1;
        breastBot =
            transformCurve(outMidTight, breastBot, outMidTight, breastBotTight, this.cling);

        if (breastTip.y > ex.waist.y) {
            startPoint = ex.lat;
            endPoint = ex.waist;
        } else {
            startPoint = ex.waist;
            endPoint = ex.hip;
        }
        const breastTipTight = interpolateCurve(startPoint,
            endPoint,
            {
                x: null,
                y: breastTip.y
            });

        const tipAdjust = (breastTipTight.x - breastTip.x) * this.cling;
        breastTip = adjust(breastTip, tipAdjust, 0);
        outTop.cp1 += tipAdjust;
        outTop.cp2 += tipAdjust;
    } else {
        mid = {
            x: ex.waist.x * 0.5,
            y: ex.waist.y
        };
        mid.cp1 = simpleQuadratic(top, mid, 0.5, 3);
        bot.cp1 = simpleQuadratic(mid, bot, 0.5, -5);

        outMid = extractPoint(ex.waist);
        outMid.cp1 = ex.hip.cp2;
        outMid.cp2 = ex.hip.cp1;
        outMid = adjust(outMid, outMid.x * 0.05, 0);

        outTop = extractPoint(ex.armpit);
    }
    outBot.cp1 = null;
    outBot.cp2 = null;
    return {
        collarbone,
        top,
        mid,
        bot,
        outBot,
        outMid,
        breastBot,
        breastTip,
        outTop
    };
}

/**
 * Long sleeve means between elbow and wrist
 */
export class MediumLooseSleevePart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.ARMS,
            loc       : "arm",
            aboveParts: ["parts arm", "decorativeParts arm", "parts hand"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {

        const collarbone = extractPoint(ex.collarbone);
        collarbone.x -= 0.3;

        let shoulder;
        if (ex.deltoids) {
            shoulder = adjust(ex.deltoids, 1, 0);
            shoulder.x += 2;
        } else {
            shoulder = adjust(ex.shoulder, 1, 0);
        }
        shoulder.x += 1;

        const bot = {
            x: ex.armpit.x * 0.5 + ex.elbow.in.x * 0.5,
            y: ex.elbow.in.y
        };
        const outBot = {
            x: ex.elbow.out.x + 4,
            y: bot.y + 2
        };
        bot.cp1 = {
            x: outBot.x * 0.7 + bot.x * 0.3,
            y: outBot.y
        };
        bot.cp2 = {
            x: outBot.x * 0.3 + bot.x * 0.7,
            y: bot.y - 1
        };

        const {outTop} = calcJacket.call(
            this,
            ex);
        outTop.x -= 0.3;

        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            collarbone,
            shoulder,
            outBot,
            bot,
            outTop
        );
        ctx.fill();
        ctx.stroke();

        setStrokeAndFill(ctx,
            {
                stroke: this.liningPattern,
                fill  : none
            },
            ex);
        ctx.lineWidth = this.liningWidth;
        ctx.clip();
        ctx.beginPath();
        drawPoints(ctx,
            outBot,
            bot
        );
        ctx.stroke();

    }
}


export class Jacket extends Clothing {
    constructor(...data) {
        super({
            clothingLayer  : Clothes.Layer.MID,
            /**
             * How much of the stomach should be covered (1 means fully)
             */
            stomachCoverage: 0.8,
            /**
             * How much of the chest should be covered (horizontally)
             */
            chestCoverage  : 0.5,
            /**
             * How far to extend the sleeve (between 0 and 1)
             */
            sleeveLength   : 0.8,
            /**
             * How wide the patterned lining is
             */
            liningWidth    : 1,
            liningPattern  : "#000",
            thickness      : 0.8,
            /**
             * How tightly it clings to the body
             */
            cling          : 0.4,
        }, ...data);
    }
}


export class LooseJacket extends Jacket {
    constructor(...data) {
        super({
            stomachCoverage: 0.7,
            sleeveLength   : 1,
        }, ...data);
    }


    fill() {
        return "hsl(0,10%,20%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: JacketBasePart
            },
            {
                side: Part.LEFT,
                Part: MediumLooseSleevePart
            },
            {
                side: Part.RIGHT,
                Part: MediumLooseSleevePart
            },
        ];
    }
}
