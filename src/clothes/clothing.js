import {none, clone} from "drawpoint";
import {extractUnmodifiedLocation, extractLocationModifier, locationIsSideless} from "../util/part";
import {getSideLocation, getSideValue} from "../parts/part";
import {Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";

/**
 * Where all Clothing and ClothingPart should go
 * @namespace Clothes
 * @memberof module:da
 */

/**
 * Clothing holds statistics and information about the clothes, but no drawing methods
 * Instead, it holds ClothingParts that each know how to draw themselves
 * @memberof module:da
 */
export class Clothing {
    constructor(...data) {

        /**
         * Default properties of every clothing object
         * @property {module:da.Clothes.Layer} clothingLayer Layer relative to other clothing (how close
         * is the clothing to the skin)
         * @property {object[]} partPrototypes Pairs of side and ClothingPart prototypes
         * @property {module:da.Layer} partPrototypes[].side Side of the clothing part
         * @property {module:da.ClothingPart} partPrototype[].part A clothing part used by this clothing
         * @type {{clothingLayer: number, partPrototypes: Array}}
         */
        Object.assign(this, {
            clothingLayer: Clothes.Layer.BASE,
            thickness    : 1.2,
            /**
             * Whether this piece of clothing explicitly does not cover whatever is beneath it
             * even when the part has coverConceal populated. (e.g. hair clips shouldn't hide
             * the hair parts its attached to)
             */
            noCover      : false,
        });
        // actual parts storage
        this.parts = [];

        // replace each part (prototype) with an actual part object
        Object.getPrototypeOf(this).partPrototypes.forEach((partPair) => {
            let {side, Part} = partPair;

            let partData = {};
            Object.assign(partData, ...data.map(d => d[Part.name]));
            // allow passing down data to part creation
            const thisPart = new Part(partData);
            // mixin behaviour by assuming all component part's properties
            // TODO blacklist certain properties to be assigned (aboveParts, belowParts - parts specific info)
            Object.assign(this, thisPart);
            processClothingPartLocation(side, thisPart);
            thisPart._owner = this;

            this.parts.push(thisPart);
        });

        // do this after all parts have their default properties assigned
        Object.assign(this, ...data);
        this.side = getSideValue(this.side);
        // for reviving where parts is assigned from data rather than created
        this.parts.forEach((thisPart) => {
            thisPart._owner = this;
        })
    }

    stroke() {
        return none;
    }

    fill() {
        return "#fff";
    }
}

export function processClothingPartLocation(side, part) {
    if (part.forcedSide !== undefined) {
        side = part.forcedSide;
    }

    const sideString = getSideLocation(side);

    if (part.forcedNoSideString === false) {
        // rename for more specificity
        if (sideString === "right" ||
            sideString === "left") {
            const baseLocation = extractUnmodifiedLocation(part.loc);
            if (locationIsSideless(baseLocation) === false) {
                const locationModifiers = extractLocationModifier(part.loc);
                part.loc = locationModifiers + sideString + " " + baseLocation;
            }
        }
    }

    part.side = getSideValue(side);
    return part;
}

export const Clothes = {
    /**
     * Clothing layers (separate from drawing layers)
     * @readonly
     * @enum
     * @memberof module:da.Clothes
     */
    Layer: Object.freeze({
        BASE      : 0,
        INNER     : 1,
        MID       : 2,
        OUTER     : 3,
        NUM_LAYERS: 4,
    }),
    /**
     * Create a Clothing instance
     * @memberof module:da.Clothes
     * @param {Clothing} ClothingClass Clothing prototype to instantiate
     * @param {object} data Overriding data
     * @returns {Clothing} Instantiated clothing object
     */
    create(ClothingClass, ...data) {
        return new ClothingClass(...data);
    },

    simpleStrokeFill(ctx, ex, clothing) {
        setStrokeAndFill(ctx,
            {
                fill  : clothing.fill,
                stroke: clothing.stroke
            },
            ex);
        ctx.lineWidth = clothing.thickness;
    },
};


/**
 * Holds no statistical information (state)
 * instead relying on the owning Clothing object to do so
 * @memberof module:da
 */
export class ClothingPart {
    constructor(...data) {
        Object.assign(this, {
            // drawing layer (not clothing layer)
            layer             : Layer.FRONT,
            loc               : "torso",
            /**
             * When this ClothingPart has a side, whether to force it to not append
             * a side to its location (e.g. if it's torso and you want it to appear on a side,
             * but don't want the location to be "left torso")
             */
            forcedNoSideString: false,
        }, ...data);
    }

    renderClothingPoints() {
    }
}


