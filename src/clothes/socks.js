import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Part} from "../parts/part";
import {Layer} from "../util/canvas";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
} from "drawpoint";

export class SockPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "feet",
            aboveParts: ["parts feet", "parts leg"],
        }, ...data);
    }
}


export class ShortSockPart extends SockPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {

        const outTop = extractPoint(ex.ankle.out);
        const out = ex.ankle.outbot;
        const outBot = ex.toe.out;
        const inBot = ex.toe.in;
        const inner = ex.ankle.inbot;
        const inTop = ex.ankle.in;

        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, -3);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);


        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop, out, outBot, inBot, inner, inTop, outTop
        );
        ctx.fill();
        ctx.stroke();

    }
}

export class MediumSockPart extends SockPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {
        let outT = 1 - this.length;
        if (this.length > 0.75) {
            outT += (this.length - 0.75) * 0.3;
        }

        let sp = splitCurve(outT, ex.calf.out, ex.ankle.out);
        const outTop = sp.left.p2;
        const out = sp.right.p2;

        sp = splitCurve(this.length, ex.ankle.in, ex.calf.in);
        const inTop = sp.left.p2;

        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, -2.5 + this.length);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop,
            out,
            ex.ankle.outbot,
            ex.toe.out,
            ex.toe.in,
            ex.ankle.inbot,
            ex.ankle.in,
            inTop,
            outTop
        );
        ctx.fill();
        ctx.stroke();

    }
}

export class LongSockPart extends SockPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {
        let outT = 1 - this.length;

        let sp = splitCurve(outT, ex.knee.out, ex.calf.out);
        const outTop = sp.left.p2;
        const out = sp.right.p2;

        let inT = this.length;
        if (this.length < 0.7) {
            inT += (0.7 - this.length) * 0.3;
        }
        sp = splitCurve(inT, ex.calf.in, ex.knee.in);
        const inTop = sp.left.p2;

        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, -2.5 + this.length);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop,
            out,
            ex.ankle.out,
            ex.ankle.outbot,
            ex.toe.out,
            ex.toe.in,
            ex.ankle.inbot,
            ex.ankle.in,
            ex.calf.in,
            inTop,
            outTop
        );
        ctx.fill();
        ctx.stroke();

    }
}

export class ThighHighPart extends SockPart {
    constructor(...data) {
        super(...data);
    }

    renderClothingPoints(ex, ctx) {
        let outT = 0.5 - this.length * 0.5;

        let sp = splitCurve(outT, ex.thigh.out, ex.knee.out);
        const outTop = sp.left.p2;
        const out = sp.right.p2;

        let inT = this.length * 0.5;
        // if (this.length < 0.7) {
        //     inT += (0.7 - this.length) * 0.3;
        // }
        sp = splitCurve(inT, ex.thigh.in, ex.thigh.top);
        const inTop = sp.left.p2;

        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, 2.5 + this.length);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx, ex, this);

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop,
            out,
            ex.calf.out,
            ex.ankle.out,
            ex.ankle.outbot,
            ex.toe.out,
            ex.toe.in,
            ex.ankle.inbot,
            ex.ankle.in,
            ex.calf.in,
            ex.knee.in,
            ex.knee.intop,
            inTop
        );
        ctx.stroke();
        drawPoints(ctx, null, outTop);
        ctx.fill();

    }
}

export class ThighHighBandPart extends SockPart {
    constructor(...data) {
        super({
            aboveSameLayerParts: ["feet"]
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        let outT = 0.5 - this.length * 0.5 - this.bandWidth * 0.03;

        let sp = splitCurve(outT, ex.thigh.out, ex.knee.out);
        const outTop = sp.left.p2;
        sp = splitCurve(0.1 * this.bandWidth, outTop, ex.knee.out);
        const out = sp.left.p2;

        let inT = this.length * 0.5 + this.bandWidth * 0.03;
        sp = splitCurve(inT, ex.thigh.in, ex.thigh.top);
        const inTop = sp.left.p2;
        sp = splitCurve(0.2 * this.bandWidth, ex.thigh.in, inTop);
        const inBot = sp.left.p2;
        inTop.cp1 = sp.right.p2.cp1;
        inTop.cp2 = sp.right.p2.cp2;

        inBot.cp1 = simpleQuadratic(out, inBot, 0.5, -2.5 - this.length);
        outTop.cp1 = simpleQuadratic(inTop, outTop, 0.5, 2.5 + this.length);

        // scale to simulate clothing thickness
        Clothes.simpleStrokeFill(ctx,
            ex,
            {
                stroke: this.bandPattern,
                fill  : this.bandPattern
            });

        // top strap
        ctx.beginPath();
        drawPoints(ctx,
            outTop,
            out,
            inBot, inTop, outTop
        );
        ctx.fill();
        ctx.stroke();

    }
}


export class Sock extends Clothing {
    constructor(...data) {
        super({
            clothingLayer: Clothes.Layer.BASE,
            length       : 0.5,
            thickness    : 1,
            stroke       : "#b1a693",
        }, ...data);
    }
}


export class ShortSocks extends Sock {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: ShortSockPart
            },
            {
                side: Part.RIGHT,
                Part: ShortSockPart
            },
        ];
    }
}


export class MediumSocks extends Sock {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: MediumSockPart
            },
            {
                side: Part.RIGHT,
                Part: MediumSockPart
            },
        ];
    }
}


export class LongSocks extends Sock {
    constructor(...data) {
        super(...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: LongSockPart,
            },
            {
                side: Part.RIGHT,
                Part: LongSockPart,
            },
        ];
    }
}


export class ThighHighs extends Sock {
    constructor(...data) {
        super({
            bandWidth  : 2,
            bandPattern: "#000",
        }, ...data);
    }

    get partPrototypes() {
        return [
            {
                side: Part.LEFT,
                Part: ThighHighPart,
            },
            {
                side: Part.RIGHT,
                Part: ThighHighPart,
            },
            {
                side: Part.RIGHT,
                Part: ThighHighBandPart,
            },
        ];
    }
}

