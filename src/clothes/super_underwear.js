import {Clothes, ClothingPart, Clothing} from "./clothing";
import {Underwear,calcBra} from "./underwear";
import {seamWidth, Layer} from "../util/canvas";
import {setStrokeAndFill} from "../util/draw";
import {
    simpleQuadratic,
    drawPoints,
    extractPoint,
    splitCurve,
    clone,
	adjust,
	breakPoint,
	interpolateCurve,
} from "drawpoint";

import {
	lineLineIntersection,
	pointLineIntersection,
} from "../util/auxiliary";


export function calcSuperBra(ex) {
    //from calcBra
	if (ex.hasOwnProperty("breast") === false) {
        return null;
    }
    const sp = splitCurve(0.7, ex.breast.top, ex.breast.tip);
    const bra = {
        out: clone(extractPoint(sp.right.p1)),
        tip: clone(extractPoint(sp.right.p2))
    };
    bra.top = {
        x: ex.breast.bot.x,
        y: bra.out.y + 2
    };
	
	//from BraPart
	bra.out.cp1 = simpleQuadratic(bra.top, bra.out, 0.4, 1);
	bra.top.cp1 = simpleQuadratic(ex.breast.cleavage, bra.top, 0.6, 2);
		
		
	//wip
	bra.bot = adjust(ex.breast.bot, 0,0);
	bra.cleavage = adjust(ex.breast.cleavage, 0, 0);
	bra.inner = adjust(ex.breast.in, 0, 0);	

	return bra;
}

export function calcSuperBraStrap(ex,neckCoverage,strapWidth) {
	//BOTTOM STRAP
	const bra = calcBra(ex);
	let strap = {};
	strap.out = {
		x: ex.breast.bot.x,
		y: ex.breast.cleavage.y
	};
	strap.outbot = {
		x: ex.breast.bot.x,
		y: ex.breast.bot.y + 3
	};
	strap.bot = {
		x: -seamWidth,
		y: ex.breast.bot.y + 5
	};
	strap.mid = {
		x: -seamWidth,
		y: ex.breast.cleavage.y
	};
  
	
	//TOP STRAP 
	let sp = splitCurve((1-neckCoverage), ex.neck.cusp, ex.collarbone );
	strap.strapTop = sp.left.p2;
	strap.strapTop.y-=(strapWidth/2);
	
	strap.breastTop =clone(ex.breast.top);
	strap.breastTop.cp1 = {
		x: strap.breastTop.x - 5,
		y: strap.breastTop.y + 10
	};
	
	strap.breastOut = extractPoint(bra.out);
	strap.breastOut.x-=2;
	strap.breastOut.y+=1;
		
	return strap;
}


export class SuperBraGenitalPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.GENITALS,
            loc       : "chest",
            reflect   : true,
            aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        const bra = calcSuperBra(ex);
        if (bra === null) {
            return;
        }
		const strap = calcSuperBraStrap(ex,this.neckCoverage,this.strapWidth);
		
		setStrokeAndFill(ctx, {
                fill  : this.fill,
                stroke: this.stroke
            },
        ex);
		ctx.lineWidth = this.thickness;
		
		//BRA
		//not sure why they are adjusted this way but dress is using this 
		const top = adjust(ex.breast.top, 0.1, 0.2);
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		const inner = adjust(ex.breast.in, -0.4, 0);
		
		ctx.save();	
			ctx.beginPath();
			drawPoints(ctx, top,tip,bot,inner);
			ctx.clip();
		
			ctx.beginPath();
			drawPoints(ctx,
				bra.top,
				bra.out,
				bra.tip,
				bra.bot,
				bra.inner,
				bra.cleavage,
				bra.top
			);
			ctx.fill();
			ctx.stroke();
		ctx.restore();
		
		//NIPPLE
		let temp = interpolateCurve(inner,top,{x:null,y:ex.chest.nipples.y});
		if(!temp||!temp[0]||temp[0].x>ex.chest.nipples.x){
			ctx.lineWidth = 3;
			ctx.strokeStyle = ctx.fillStyle;
			
			ctx.beginPath();
			drawPoints(ctx, breakPoint, ex.chest.nipples);
			ctx.stroke();
		}

		//TOP STRAP
		if(this.showStrap){
			ctx.save();
			ctx.lineWidth = this.strapWidth; // top strap
			ctx.strokeStyle = this.highlight;
			ctx.beginPath();
			drawPoints(ctx,strap.breastTop,strap.breastOut);
			ctx.stroke();
			ctx.restore();
		}	
    }
}


export class SuperBraChestPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "+torso",
            reflect   : true,
            aboveParts: ["parts torso"],
			belowParts: ["parts chest"]
			//aboveParts: ["parts chest", "decorativeParts chest"],
        }, ...data);
    }


    renderClothingPoints(ex, ctx) {
        const bra = calcSuperBra(ex);
        if (bra === null) {
            return;
        }
		const strap = calcSuperBraStrap(ex,this.neckCoverage,this.strapWidth);
		
		setStrokeAndFill(ctx, {
                fill  : this.fill,
                stroke: this.stroke
            },
        ex);
		ctx.lineWidth = this.thickness;
		
		//BOTTOM STRAP
		ctx.beginPath();
		drawPoints(ctx, strap.out, strap.outbot, strap.bot, strap.mid, strap.out);
		ctx.fill();
				
		//TOP STRAP
		if(this.showStrap){
			ctx.save();
			ctx.lineWidth = this.strapWidth; // top strap
			ctx.strokeStyle = this.highlight;
			ctx.beginPath();
			drawPoints(ctx, strap.strapTop, strap.breastTop);
			ctx.stroke();
			ctx.restore();
		}
		
		//BRA
		//not sure why they are adjusted this way but dress is using this 
		const top = adjust(ex.breast.top, 0.1, 0.2);
		const tip = adjust(ex.breast.tip, 0.1, 0);
		const bot = adjust(ex.breast.bot, 0, -0.1);
		const inner = adjust(ex.breast.in, -0.4, 0);
		
		ctx.save();	
			ctx.beginPath();
			drawPoints(ctx, top,tip,{x:0,y:tip.y-50},{x:0,y:top.y+50},{x:top.x,y:top.y+50});
			ctx.clip();
		
			ctx.beginPath();
			drawPoints(ctx,
				bra.top,
				bra.out,
				bra.tip,
				bra.bot,
				bra.inner,
				bra.cleavage,
				bra.top
			);
			ctx.fill();
			ctx.stroke();
		ctx.restore();
			
    }
}


export class SuperPantiesPart extends ClothingPart {
    constructor(...data) {
        super({
            layer     : Layer.FRONT,
            loc       : "groin",
            reflect   : true,
            aboveParts: ["parts groin", "parts torso","parts leg"],
            belowSameLayerParts: ["clothingParts leg"],
        }, ...data);
    }

    renderClothingPoints(ex, ctx) {
        if (ex.hasOwnProperty("groin") === false) {
            return;
        }
		
		const panties = {};
		let temp;
		
		//side upper point
		if(this.waistCoverage>0){
			temp = splitCurve((1-this.waistCoverage), ex.waist,ex.hip);
			panties.sideTop = extractPoint(temp.left.p2);
		}else{
			temp = splitCurve(Math.abs(this.waistCoverage),ex.hip,ex.thigh.out);
			panties.sideTop = extractPoint(temp.left.p2);
		}
		panties.sideTop = adjust(panties.sideTop, -(this.thickness * 0.5), 0);
		
		
		//side lower point 
		//top always above bot 
		if(this.waistCoverageLower>this.waistCoverage)this.waistCoverageLower=this.waistCoverage;
		if(this.waistCoverageLower>0){
			temp = splitCurve((1-this.waistCoverageLower),ex.waist,ex.hip);	
			panties.sideBot = temp.left.p2;
		}else{
			temp = splitCurve(Math.abs(this.waistCoverageLower),ex.hip,ex.thigh.out);	
			panties.sideBot = temp.left.p2;
		}
		panties.sideBot = adjust(panties.sideBot, -(this.thickness * 0.5), 0);
		
		//top & curve
		panties.top = {x:-0.1,y:(panties.sideTop.y+this.topY-8)}; 
		
		panties.sideTop.cp1 = {
			x: (panties.sideTop.x * 0.5) + (panties.top.x * 0.5),
			y: panties.sideTop.y,
		};
		panties.sideTop.cp1.x+=this.curveTopX;
		panties.sideTop.cp1.y+=this.curveTopY-9;
		
		//bottom
		panties.bot = adjust(clone(ex.groin),-0.1,0);		
		
		temp = splitCurve(this.genCoverage,ex.groin,extractPoint(ex.thigh.top));	
		panties.botOut = extractPoint(temp.left.p2);
		
		//bottom curve
		temp = splitCurve(0.5,panties.sideBot,panties.botOut);	
		panties.botOut.cp1 = extractPoint(temp.left.p2);
		panties.botOut.cp1.x+=this.curveBotX-9;
		panties.botOut.cp1.y+=this.curveBotY+5;


        Clothes.simpleStrokeFill(ctx, ex, this);
        ctx.beginPath();
        drawPoints(ctx,
            panties.top,
			panties.sideTop,
			panties.sideBot,
			panties.botOut,
			panties.bot
                   
           // panties.top
		);
        ctx.fill();
		/*
		ctx.beginPath();
        drawPoints(ctx,
            panties.top,
			panties.sideTop,
			panties.sideBot,
			panties.botOut,
			panties.bot,
           //panties.top
		);
		*/
		ctx.stroke();
    }
}


export class SuperBra extends Underwear {
    constructor(...data) {
        super({
			showStrap:false,
			strapWidth:2,
			neckCoverage:0.7,
			thickness:0.5,
			highlight:"hsl(346, 50%, 70%)",
		},...data);
    }
	/*
	stroke() {
        return "hsl(346, 50%, 70%)";
    }
	*/
	
 
	fill() {
        return "hsl(346, 57%, 82%)";
    }
	
	get partPrototypes() {
        return [
             
            {
                side: null,
                Part: SuperBraChestPart
            },
            {
                side: null,
                Part: SuperBraGenitalPart
            }
        ];
    }
}


export class SuperPanties extends Underwear {
    constructor(...data) {
        super({
            waistCoverage:0.11,
			waistCoverageLower:-0.05,
			genCoverage:1,
			topY:0,
			curveTopX:0,
			curveTopY:0,
			curveBotX:0,
			curveBotY:0,
        }, ...data);
    }

    fill() {
        return "hsl(346, 57%, 82%)";
    }

    get partPrototypes() {
        return [
            {
                side: null,
                Part: SuperPantiesPart
            }
        ];
    }
}