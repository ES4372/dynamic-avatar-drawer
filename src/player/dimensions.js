import {styles} from "../hair_parts/styles";

/**
 * Any physical property that needs calculation and is tangible should go in here
 */
export const baseDimDesc = {
    human: {
        areolaSize   : {
            linkedPart: "chest",
            units     : "mm",
            low       : 0,
            high      : 50,
            avg       : 20,
            stdev     : 2,
            bias      : 3,
        },
        armThickness : {
            linkedPart: "arm",
            units     : "mm",
            low       : 45,
            high      : 95,
            avg       : 65,
            stdev     : 3,
            bias      : -7,
            calc() {
                const base = this.getDim("armThickness") - this.get("fem") * 0.5;
                // scale by height so that shorter people have shorter arm length
                return adjustLengthByHeight(base, this.getDim("height"), 0.2);
            },
        },
        armLength    : {
            linkedPart: "arm",
            units     : "cm",
            low       : 30,
            high      : 80,
            avg       : 45,
            stdev     : 2,
            bias      : 0,
            calc() {
                const base = this.getDim("armLength");
                // scale by height so that shorter people have shorter arm length
                return adjustLengthByHeight(base, this.getDim("height"));
            },
        },
        breastSize   : {
            linkedPart: "chest",
            units     : "cm",
            low       : -10,
            high      : 50,
            avg       : -1,
            stdev     : 5,
            bias      : 20,
            calc() {
                return this.getDim("breastSize") + this.get("fem") * 0.1;
            },
        },
        buttFullness : {
            linkedPart: "butt",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 10,
            stdev     : 2,
            bias      : 4,
            calc() {
                return this.getDim("buttFullness") + this.get("fem") * 0.2;
            },
        },
        chinWidth    : {
            linkedPart: "head",
            units     : "mm",
            low       : 30,
            high      : 140,
            avg       : 70,
            stdev     : 2,
            bias      : -2
        },
        eyelashLength: {
            linkedPart: "eyelash",
            units     : "mm",
            low       : 0,
            high      : 15,
            avg       : 3,
            stdev     : 2,
            bias      : 3,
        },
        eyeSize      : {
            linkedPart: "eyes",
            units     : "mm",
            low       : 0,
            high      : 40,
            avg       : 15,
            stdev     : 2,
            bias      : 3,
        },
        faceFem      : {
            linkedPart: "head",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 15,
            stdev     : 2,
            bias      : 14,
            calc() {
                return this.getDim("faceFem") + this.get("fem") * 0.5;
            },
        },
        // how long is your face
        faceLength   : {
            linkedPart: "head",
            units     : "mm",
            low       : 180,
            high      : 270,
            avg       : 230,
            stdev     : 5,
            bias      : -2,
        },
        // defined as relative to the center of the face (so face half width)
        faceWidth    : {
            linkedPart: "head",
            units     : "mm",
            low       : 75,
            high      : 105,
            avg       : 93,
            stdev     : 2,
            bias      : -2,
            calc() {
                return this.getDim("faceWidth") - this.get("fem") * 0.5;
            },
        },


        hairLength: {
            linkedPart: "hair",
            units     : "cm",
            low       : 0,
            high      : 110,
            avg       : 6,
            stdev     : 2,
            bias      : 10,
            calc() {
                return this.getDim("hairLength") + this.get("fem");
            },
        },
        hairStyle : {
            linkedPart: "hair",
            desc      : "Index of base hair style to be worn (individual parts can be swapped afterwards)",
            units     : "index",
            low       : 0,
            high      : styles.length - 1,
            avg       : 1,
            stdev     : 1,
            bias      : 0,
            // needs to be integer
            calc() {
                return Math.round(this.getDim("hairStyle"));
            },
        },


        hairHue       : {
            linkedPart: "hair",
            units     : "degree",
            low       : 0,
            high      : 360,
            avg       : 30,
            stdev     : 30,
            bias      : 0,
        },
        hairSaturation: {
            linkedPart: "hair",
            units     : "%",
            low       : 0,
            high      : 100,
            avg       : 50,
            stdev     : 10,
            bias      : 0,
        },
        hairLightness : {
            linkedPart: "hair",
            units     : "%",
            low       : 0,
            high      : 100,
            avg       : 30,
            stdev     : 10,
            bias      : 0,
        },
        handSize      : {
            linkedPart: "hand",
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 100,
            stdev     : 5,
            bias      : -10,
            calc() {
                const base = this.getDim("handSize") - this.get("fem") * 0.3;
                return adjustLengthByHeight(base, this.getDim("height"), 0.4);
            }
        },
        height        : {
            units: "cm",
            low  : 50,
            high : 270,
            avg  : 167,
            stdev: 3,
            bias : -5,
        },
        hipWidth      : {
            linkedPart: "torso",
            units     : "mm",
            low       : 70,
            high      : 200,
            avg       : 125,
            stdev     : 2.5,
            bias      : 3,
            calc() {
                return this.getDim("hipWidth") + this.get("fem") * 2;
            },
        },


        legFem     : {
            linkedPart: "leg",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 13,
            stdev     : 2,
            bias      : 9,
            calc() {
                return this.getDim("legFem") + this.get("fem") * 0.6;
            }
        },
        legFullness: {
            linkedPart: "leg",
            desc      : "Approximately how thick the leg is as a combination of fat and muscle",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 10,
            stdev     : 3,
            bias      : 2,
            calc() {
                return this.getDim("legFullness") + this.get("fem") * 0.5;
            },
        },
        legLength  : {
            linkedPart: "leg",
            units     : "cm",
            low       : 20,
            high      : 200,
            avg       : 95,
            stdev     : 2.5,
            bias      : 4,
            calc() {
                const base = this.getDim("legLength");
                return adjustLengthByHeight(base, this.getDim("height"));
            },
        },
        lipSize    : {
            linkedPart: "lips",
            units     : "mm",
            low       : 0,
            high      : 40,
            avg       : 14,
            stdev     : 1.5,
            bias      : 2,
            calc() {
                return this.getDim("lipSize") + this.get("fem") * 0.05;
            },
        },
        lowerMuscle: {
            linkedPart: "leg",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 16,
            stdev     : 3,
            bias      : -17,
            calc() {
                return this.getDim("lowerMuscle") - this.get("fem");
            },
        },


        neckLength: {
            linkedPart: "neck",
            units     : "mm",
            low       : 0,
            high      : 120,
            avg       : 72,
            stdev     : 5,
            bias      : 0,
        },
        neckWidth : {
            linkedPart: "neck",
            units     : "mm",
            low       : 35,
            high      : 75,
            avg       : 45,
            stdev     : 2,
            bias      : -2,
        },


        penisSize    : {
            linkedPart: "genitals",
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 20,
            stdev     : 5,
            bias      : -20,
            calc() {
                const baseSize = this.getDim("penisSize") - this.get("fem") * 0.5;
                return baseSize + this.getMod("arousal") * 0.5;
            }
        },
        shoulderWidth: {
            linkedPart: "chest",
            units     : "mm",
            low       : 0,
            high      : 200,
            avg       : 73,
            stdev     : 2,
            bias      : -7,
            calc() {
                const base = this.getDim("shoulderWidth") - this.get("fem") * 0.3;
                return adjustLengthByHeight(base, this.getDim("height"), 0.4);
            }
        },
        // translucent (-20) to porcelein (-10) to fair (-5) to tanned (5) to brown (15) pure
        // black (50)
        skin         : {
            units: "arbitrary",
            low  : -20,
            high : 50,
            avg  : 7,
            stdev: 5,
        },
        testicleSize : {
            linkedPart: "genitals",
            units     : "mm",
            low       : 0,
            high      : 100,
            avg       : 35,
            stdev     : 4,
            bias      : -20,
            calc() {
                return this.getDim("testicleSize") - this.get("fem");
            }
        },
        upperMuscle  : {
            linkedPart: "chest",
            units     : "arbitrary",
            low       : 0,
            high      : 40,
            avg       : 14,
            stdev     : 3,
            bias      : -15,
            calc() {
                return this.getDim("upperMuscle") - this.get("fem") * 0.5;
            },
        },
        vaginaSize   : {
            linkedPart: "genitals",
            units     : "mm",
            low       : 0,
            high      : 100,
            avg       : 40,
            stdev     : 10,
            bias      : 5,
            calc() {
                return this.getDim("vaginaSize") + this.Mods.labiaFullness +
                       this.Mods.arousal * 0.2;
            },
        },
        waistWidth   : {
            linkedPart: "torso",
            units     : "mm",  //(center to closest point horizontally)
            low       : 70,
            high      : 150,
            avg       : 120,
            stdev     : 3,
            bias      : -3,
            calc() {
                const base = this.getDim("waistWidth") - this.get("fem");
                return base + this.getDim("upperMuscle");
            },
        },
    },
};


function adjustLengthByHeight(base, height, weight = 1) {
    return base * (1 - weight) + base * weight * height / 170;
}

// first element of discrete pools is the default
export const basedimDiscretePool = {
    eyecolor: ["white"],
};

/**
 * Dimension calculation based on avatar statistics. These are callback methods that
 * the user should define to link extended gameplay stats on the Player to physical dimensions.
 * @callback dimensionCalculation
 * @this {module:da.Player} avatar object
 * @param {number} base this dimension as calculated by all previous calculations
 */

/**
 * Extend the way a dimension is calculated to plug in a user statistics system
 * @memberof module:da
 * @param {(Object|string)} dimDesc Either the dimension descriptor object, or
 * a string in the format of "skeleton.dimension"
 * @param {dimensionCalculation} newCalc User method for calculating dimension
 */
export function extendDimensionCalc(dimDesc, newCalc) {
    if (typeof dimDesc === "string") {
        const [skeleton, dim] = dimDesc.split(".");
        dimDesc = baseDimDesc[skeleton][dim];
    }

    const oldCalc = dimDesc.calc;

    /**
     * 'this' argument should be the avatar
     * @param args
     */
    dimDesc.calc = function (...args) {
        let base;
        if (oldCalc) {
            base = oldCalc.call(this, ...args);
        } else {
            base = this.getDim(dimDesc.name);
        }
        return newCalc.call(this, base, ...args);
    };
}

export function loadDimensionDescriptions() {
    for (let skeleton in baseDimDesc) {
        if (baseDimDesc.hasOwnProperty(skeleton) === false) {
            continue;
        }
        const description = baseDimDesc[skeleton];
        for (let dim in description) {
            if (description.hasOwnProperty(dim)) {
                description[dim].name = dim;
            }
        }
    }
}

