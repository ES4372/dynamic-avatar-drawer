import {shadingMedium, ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    adjustPoints,
    extractPoint,
    simpleQuadratic,
    clone,
    adjust,
    breakPoint,
    endPoint,
    reverseDrawPoint,
    splitCurve,
    rotatePoints
} from "drawpoint";

class LeftArmShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "left arm",
            layer: Layer.ARMS,
        }, ...data);
    }

    fill() {
        return shadingMedium;
    }

    calcDrawPoints(ex) {
        const points = [];
        const [elbow, armpit] = adjustPoints(this.armThickness * 0.07,
            0,
            ex.elbow.in,
            ex.armpit);

        // we'll start shading from hand if it exists
        if (ex.hand) {
            const [wrist, thumbOut] = adjustPoints(this.armThickness * 0.02,
                0,
                ex.wrist.in,
                ex.thumb.out);
            const thumbTip = extractPoint(ex.thumb.tip);

            points.push(thumbTip, thumbOut, wrist);
        } else {
            const wrist = extractPoint(ex.wrist.in);
            points.push(wrist);
        }


        let inTop;
        if (ex.breast) {
            inTop = extractPoint(ex.breast.top);
            inTop.cp1 = simpleQuadratic(armpit, inTop, 0.5, -4);
        } else {
            inTop = extractPoint(ex.armpit);
        }

        const armpitOut = extractPoint(ex.armpit);
        const elbowIn = extractPoint(ex.elbow.in);
        elbowIn.cp1 = clone(ex.armpit.cp2);
        elbowIn.cp2 = clone(ex.armpit.cp1);
        const wristIn = extractPoint(ex.wrist.in);
        wristIn.cp1 = clone(ex.elbow.in.cp2);
        wristIn.cp2 = clone(ex.elbow.in.cp1);
        points.push(elbow, armpit, inTop, armpitOut, elbowIn, wristIn);

        if (ex.hand) {
            const thumbOut = extractPoint(ex.thumb.out);
            thumbOut.cp1 = clone(ex.wrist.in.cp2);
            thumbOut.cp2 = clone(ex.wrist.in.cp1);
            const thumbTip = extractPoint(ex.thumb.tip);
            thumbTip.cp1 = clone(ex.thumb.out.cp2);
            thumbTip.cp2 = clone(ex.thumb.out.cp1);
            points.push(thumbOut, thumbTip);

            // hand tip to palm shadow
            const handTip = extractPoint(ex.hand.tip);
            const handPalm = clone(ex.hand.palm);
            handTip.cp1 = adjust(ex.hand.palm.cp2, this.handSize * 0.01, 0);
            handTip.cp2 = adjust(ex.hand.palm.cp1, this.handSize * 0.01, 0);
            points.push(breakPoint, handTip, handPalm, handTip, endPoint);
        }

        return points;
    }
}


class LeftArmUnderShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+left arm",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const points = [];

        if (ex.hand) {
            const palm = adjust(ex.hand.palm, -0.5, -2);
            const thumbTip = adjust(ex.thumb.tip, -0.5, -1.5);
            points.push(
                ex.hand.tip,
                palm,
                thumbTip,
                ...adjustPoints(-2, -1, ex.thumb.out, ex.wrist.in));

        } else {
            points.push(ex.wrist.in);
        }

        const elbow = adjust(ex.elbow.in, -2, -1);

        const armpit = extractPoint(ex.armpit);
        armpit.cp1 = adjust(ex.armpit.cp1, -1, -0.5);
        armpit.cp2 = adjust(ex.armpit.cp2, -2, -0.5);

        points.push(elbow, armpit);

        // finish up the shadow on the other direction
        points.push(extractPoint(ex.elbow.out), ex.wrist.out);
        if (ex.hand) {
            points.push(ex.hand.knuckle, ex.hand.tip);
        } else {
            points.push(extractPoint(ex.wrist.in));
        }

        return points;
    }
}


class RightArmShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "right arm",
            layer: Layer.ARMS,
        }, ...data);
    }

    fill() {
        return shadingMedium;
    }

    calcDrawPoints(ex) {
        const points = [];
        const collarbone = extractPoint(ex.collarbone);
        const [deltoids, shoulder, elbow] = adjustPoints(-this.armThickness * 0.05,
            0,
            ex.deltoids,
            ex.shoulder,
            ex.elbow.out
        );
        points.push(collarbone, deltoids, shoulder, elbow);

        if (ex.hand) {
            const wrist = adjust(ex.wrist.out, -this.armThickness * 0.03, 0);
            const handKnuckle = adjust(ex.hand.knuckle, -this.armThickness * 0.02, 0);
            const handTip = clone(ex.hand.tip);
            handTip.cp1 = adjust(ex.hand.tip.cp1, -this.armThickness * 0.02, 0);

            points.push(wrist, handKnuckle);
            if (ex.hand.fist) {
                points.push(adjust(ex.hand.fist, -this.armThickness * 0.02, 1));
            }
            points.push(handTip);

            // head back up to collarbone
            if (ex.hand.fist) {
                points.push(reverseDrawPoint(ex.hand.fist, ex.hand.tip));
                points.push(reverseDrawPoint(ex.hand.knuckle, ex.hand.fist));
            } else {
                points.push(reverseDrawPoint(ex.hand.knuckle, ex.hand.tip));
            }
            points.push(reverseDrawPoint(ex.wrist.out, ex.hand.knuckle));
        } else {
            const wrist = extractPoint(ex.wrist.out);
            wrist.cp1 = {
                x: elbow.x,
                y: elbow.y
            };
            wrist.cp2 = {
                x: wrist.x - 2,
                y: wrist.y
            };
            points.push(wrist);
        }

        const outPoints = [];
        outPoints.push(reverseDrawPoint(ex.elbow.out, ex.wrist.out),
            reverseDrawPoint(ex.elbow.out, ex.elbow.out),
            reverseDrawPoint(ex.shoulder, ex.elbow.out));

        if (ex.deltoids) {
            outPoints.push(reverseDrawPoint(ex.deltoids, ex.shoulder),
                reverseDrawPoint(ex.collarbone, ex.deltoids));
        } else {
            outPoints.push(reverseDrawPoint(ex.collarbone, ex.shoulder));
        }

        points.extend(adjustPoints(3, 0, ...outPoints));
        return points;
    }
}


class RightArmUnderShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+right arm",
            layer: Layer.FRONT,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const points = [
            ex.collarbone,
            ...adjustPoints(3, -1, ex.deltoids, ex.shoulder, ex.elbow.out)
        ];

        if (ex.hand) {
            points.push(...adjustPoints(1.5,
                -0.7,
                ex.wrist.out,
                ex.hand.knuckle,
                ex.hand.tip));
            points.push(...adjustPoints(2.2, -0.9,
                ex.hand.palm,
                ex.thumb.tip,
                ex.thumb.out),
                extractPoint(ex.wrist.out)
            );
        } else {
            const wristOut = extractPoint(ex.wrist.out);
            points.push(wristOut);
        }

        points.push(extractPoint(ex.elbow.in), extractPoint(ex.collarbone));

        return points;
    }
}


class Arm extends BodyPart {
    constructor(...data) {
        super({
            loc         : "arm",
            layer       : Layer.ARMS,
            childParts  : ["hand"],
            aboveParts  : ["leg", "torso"],
            shadingParts: [
                LeftArmShading,
                LeftArmUnderShading,
                RightArmShading,
                RightArmUnderShading
            ],
        }, ...data);
    }

    clipStroke() {
    }

    clipFill() {
    }
}

export class ArmHuman extends Arm {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            let wrist = ex.wrist = {};

            // where most arm calculations should be based off of
            let armpit = ex.armpit = {
                x: ex.collarbone.x - this.upperMuscle * 0.03,
                y: ex.collarbone.y - this.torsoLength * 0.27 - this.upperMuscle * 0.1,
            };

            let shoulder = ex.shoulder = {
                x: ex.collarbone.x + this.upperMuscle * 0.2 + this.armThickness * 0.1,
                y: ex.collarbone.y - 10 - this.upperMuscle * 0.1,

            };
            shoulder.cp1 = {
                x: shoulder.x,
                y: ex.collarbone.y - 3
            };

            shoulder.cp2 = {
                x: shoulder.x - 0.5 + this.upperMuscle * 0.02,
                y: shoulder.y + 4
            };

            let elbow = ex.elbow = {};
            elbow.out = {
                x: armpit.x + 5 + this.upperMuscle * 0.17 + this.armThickness * 0.05,
                y: shoulder.y - this.armLength * 0.45,
            };

            wrist.out = {
                x  : armpit.x + 10 + this.upperMuscle * 0.01 + this.armThickness * 0.035,
                y  : elbow.out.y - this.armLength * 0.50,
                cp1: {
                    x: elbow.out.x,
                    y: elbow.out.y - 2
                },
            };
            wrist.out.cp2 = {
                x: wrist.out.x - 0.5,
                y: wrist.out.y + 3
            };

            // hand gets drawn here -----

            wrist.in = {
                x: armpit.x + 9,
                y: wrist.out.y - 1,
            };

            elbow.in = {
                x  : elbow.out.x - 3 - this.upperMuscle * 0.07 - this.armThickness * 0.05,
                y  : elbow.out.y + 2,
                cp1: {
                    x: wrist.in.x - 1,
                    y: wrist.in.y + 7
                },
            };
            // lower arm curve
            elbow.in.cp2 = {
                x: elbow.in.x + 0.5,
                y: elbow.in.y - 7,
            };
            // upper arm curve
            ex.armpit.cp1 = {
                x: elbow.in.x - 0.5,
                y: elbow.in.y + 2
            };
            ex.armpit.cp2 = {
                x: ex.armpit.x,
                y: ex.armpit.y - 2
            };

            if (this.upperMuscle > 10) {
                const bulk = this.upperMuscle - 10;
                let deltoids = ex.deltoids = {
                    x: shoulder.x - bulk * 0.12,
                    y: shoulder.y - bulk * 0.2
                };
                deltoids.cp1 = extractPoint(shoulder.cp1);
                deltoids.cp2 = extractPoint(shoulder.cp2);
                deltoids.cp1.x += bulk * 0.03;
                deltoids.cp2.x += bulk * 0.05;

                const sp = splitCurve(0.85, ex.collarbone, ex.deltoids);
                // remove curve
                shoulder = ex.shoulder = sp.right.p1;
                shoulder.cp1 = sp.right.p2.cp2;
                shoulder.cp2 = sp.right.p2.cp1;

                // add triceps
                elbow.out.cp1 = {
                    x: shoulder.x + bulk * 0.08,
                    y: shoulder.y - bulk * 0.1
                };
                elbow.out.cp2 = {
                    x: elbow.out.x,
                    y: elbow.out.y + bulk * 0.07
                };
                // biceps
                armpit.cp1.x -= bulk * 0.15;
                armpit.cp1.y += bulk * 0.1;
                armpit.cp2.x -= bulk * 0.02;
                armpit.cp2.y -= bulk * 0.1;

                // forearm
                elbow.in.cp2.x -= bulk * 0.15;
                elbow.in.cp2.y -= bulk * 0.05;
                elbow.in.cp1.y += bulk * 0.1;

                wrist.out.cp1.x += bulk * 0.1;
                wrist.out.cp1.y -= bulk * 0.1;
                wrist.out.cp2.x += bulk * 0.1;
                wrist.out.cp2.y += bulk * 0.1;

                wrist.in.x -= bulk * 0.03;
            }

            // rotate arms
            rotatePoints(elbow.in,
                mods.armRotation * Math.PI / 180,
                elbow.in,
                elbow.out,
                wrist.in,
                wrist.out);
            elbow.in.x -= Math.abs(mods.armRotation) * 0.02;
            elbow.in.y -= mods.armRotation * 0.01;
            if (mods.armRotation < -20) {
                const inwardRotation = (-20 - mods.armRotation);
                elbow.in.x -= inwardRotation * 0.05;
                elbow.in.y -= inwardRotation * 0.17;
            }
        }

        return [
            ex.collarbone,
            ex.deltoids,
            ex.shoulder,
            ex.elbow.out,
            ex.wrist.out,
            // ex.wrist.out,
            {child: "hand"},
            ex.wrist.in,
            ex.elbow.in,
            ex.armpit,
        ];
    }
}

