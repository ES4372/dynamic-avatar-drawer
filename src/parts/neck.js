import {ShadingPart} from "../draw/shading_part";
import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    adjustPoints,
    splitCurve,
    clamp,
    reflect,
    simpleQuadratic,
    clone,
} from "drawpoint";
import {averageQuadratic} from "../util/draw";

class ChinShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+neck",
            layer: Layer.GENITALS,
        }, ...data);
    }

    calcDrawPoints(ex) {
        let [skullSide, skullBot, jaw, chinOut, chinBot] = adjustPoints(0,
            -2,
            ex.skull.side,
            ex.skull.bot,
            ex.jaw,
            ex.chin.out,
            ex.chin.bot);

        let leftChinOut = reflect(chinOut);
        leftChinOut.cp1 = reflect(chinBot.cp2);
        leftChinOut.cp2 = reflect(chinBot.cp1);
        let leftJaw = reflect(jaw);
        leftJaw.cp1 = reflect(chinOut.cp2);
        leftJaw.cp2 = reflect(chinOut.cp1);

        return adjustPoints(1.3,
            0,
            ...adjustPoints(1.8, 0, skullSide, skullBot, jaw, chinOut, chinBot),
            leftChinOut,
            leftJaw);
    }
}


class NeckShading extends ShadingPart {
    constructor(...data) {
        super({
            loc  : "+neck",
            layer: Layer.GENITALS,
        }, ...data);
    }

    calcDrawPoints(ex) {
        const top = {
            x: 0,
            y: ex.neck.top.y
        };
        const bot = {
            x: ex.neck.cusp.x,
            y: ex.neck.cusp.y - 3
        };

        // bot.cp1 = simpleQuadratic(top, bot, 0.3, 3);
        bot.cp1 = ex.neck.cusp.cp1;
        const out = {
            x: ex.neck.top.x + 1,
            y: ex.neck.top.y
        };
        out.cp1 = ex.neck.cusp.cp1;
        return [top, bot, out, top];
    }
}


class Neck extends BodyPart {
    constructor(...data) {
        super({
            loc         : "neck",
            forcedSide  : null,
            layer       : Layer.FRONT,
            reflect     : true,
            shadingParts: [ChinShading, NeckShading],
        }, ...data);
    }
}


// neck will export neck.nape, neck.top, neck.cusp, trapezius.top, collarbone
export class NeckHuman extends Neck {
    constructor(...data) {
        super(...data);
    }

    calcDrawPoints(ex, mods, calculate) {
        if (calculate) {
            const neck = ex.neck = {};
            neck.nape = {
                x: 0,
                y: ex.skull.y - this.faceLength * 0.068,
            };

            // width depends on musculature
            neck.top = {
                x: this.neckWidth * 0.1 + this.upperMuscle * 0.05 - this.faceFem * 0.01,
                y: neck.nape.y
            };

            neck.cusp = {
                x: neck.top.x + 0.2,
                y: neck.top.y - this.neckLength * 0.1,
            };

            neck.cusp.cp1 = simpleQuadratic(neck.top, neck.cusp, 0.7, mods.neckCurve * 0.1);

            if (this.upperMuscle > 12) {
                let s = splitCurve(clamp(1 - (this.upperMuscle - 12) / 80, 0, 1), neck.top,
                    neck.cusp);
                ex.trapezius = s.right.p1;
            } else {
                ex.trapezius = clone(neck.cusp);
            }

            ex.collarbone = {
                x: neck.cusp.x + this.shoulderWidth * 0.1 + this.upperMuscle * 0.01,
                y: neck.cusp.y - this.torsoLength * 0.105 + this.upperMuscle * 0.05,
            };

            if (this.upperMuscle > 12) {
                ex.collarbone.x += (this.upperMuscle - 12) * 0.1;

                // curve of the trapezius
                ex.collarbone.cp1 =
                    averageQuadratic(ex.trapezius, ex.collarbone, 0.7,
                        this.upperMuscle * 0.02, this.upperMuscle * 0.02);
            } else {
                ex.collarbone.cp1 = averageQuadratic(neck.cusp, ex.collarbone, 0.3,
                    (12 - this.upperMuscle) * -0.05,
                    (12 - this.upperMuscle) * -0.05);
            }
        }
        return [ex.neck.nape, ex.neck.top, ex.neck.cusp, ex.trapezius, ex.collarbone];
    }
}
