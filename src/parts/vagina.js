import {Layer} from "../util/canvas";
import {BodyPart} from "./part";
import {
    extractPoint,
} from "drawpoint";

class Vagina extends BodyPart {
    constructor(...data) {
        super({
            loc       : "vagina",
            forcedSide: null,
            layer     : Layer.FRONT,
            aboveParts: ["parts groin"],
            belowParts: ["leg", "clothingParts groin"],
        }, ...data);
    }
}


export class VaginaHuman extends Vagina {
    constructor(...data) {
        super({
            reflect: true,
        }, ...data);
    }

    stroke() {
        return "inherit";
    }

    calcDrawPoints(ex, mods, calculate) {

        if (calculate) {
            ex.vagina = {};
            ex.coverGroin = true;


            ex.vagina.top = {
                x: ex.groin.x,
                // viewed at an angle, so length is reduced
                y: ex.groin.y + this.vaginaSize * 0.022 + mods.labiaFullness * 0.005
            };
            ex.vagina.side = extractPoint(ex.thigh.top);
            ex.vagina.side.y += 0.5 + (mods.labiaFullness * 0.1 + this.vaginaSize * 0.005);
            ex.vagina.side.x += (mods.labiaFullness * 0.02 + this.vaginaSize * 0.002);

            ex.vagina.top.cp1 = {
                x: ex.vagina.side.x,
                y: ex.vagina.side.y - 0.8 -
                   (mods.labiaFullness * 0.1 + this.vaginaSize * 0.005),
            };
            ex.vagina.top.cp2 = {
                x: ex.vagina.top.x,
                y: ex.vagina.top.y - this.vaginaSize * 0.032
            };

        }

        return [ex.vagina.side, ex.vagina.top];
    }
}

