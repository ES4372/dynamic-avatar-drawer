---
layout: post
title:  "1.22 Mascara"
date:   2019-03-02
categories: release
---

- makeup asset
    - mascara
- natural override lash fill

## Changing eyelash colour

There are now 2 ways to change eyelash colour:
 - override with `lashFill` when creatnig the avatar like `hairFill`, this represents a different
 natural eyelash colour than usual
 - wear the new Mascara Clothing item
 
## Mascara

With this you can choose how to colour the top and bottom lashes separately

{% highlight javascript %}
var mascara = da.Clothes.create(da.Mascara, {
    topFill: "rgba(200, 40, 30,0.8)",
    botFill: "rgba(30, 50, 140, 0.5)",
    extraLength: 2  // lengthen your lashes, by default 2
});
PC.wearClothing(mascara);
{% endhighlight %}

![lashes](https://i.imgur.com/7FTddKP.png)

